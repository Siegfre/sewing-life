/*
 *
 *  Slash/A's Default Instruction Set (DIS)
 *
 *  Copyright (C) 2004-2011 Artur B Adib
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <cmath>
#include <sstream>
#include "../WorldManager.h"
#include "NR-ran2.hpp"

#define WM WorldManager::Manager

#ifndef M_PI
    #define M_PI 3.14159265358979323846
#endif

#ifndef M_E
    #define M_E 2.71828182845904523536
#endif

namespace SlashA 
{
 
namespace DIS
{

class SetI : public Instruction
{
  private:
    ByteCode_Type num;
  public:
    SetI(ByteCode_Type n) : Instruction() 
    { 
      std::ostringstream nstr;
      nstr << n;
      name=nstr.str(); 
      DIS_flag = true; 
      num = n; 
    };
    ~SetI() {};
    inline void code(MemCore& core, InstructionSet& iset) { core.I = num; n_ops++; } 
};

class ItoF : public Instruction
{
  public:
    ItoF() : Instruction() { name="itof"; DIS_flag = true; };
    ~ItoF() {};
    inline void code(MemCore& core, InstructionSet& iset) 
    { 
      n_ops++;
      if (!core.setF((double)core.I))
        n_invops++;  
    }
};

class FtoI : public Instruction
{
  public:
    FtoI() : Instruction() { name="ftoi"; DIS_flag = true; };
    ~FtoI() {};
    inline void code(MemCore& core, InstructionSet& iset) 
    { 
      n_ops++; 
      core.I = (unsigned)rint(core.getF());
    }
};

class Inc : public Instruction
{
  public:
    Inc() : Instruction() { name="inc"; DIS_flag = true; };
    ~Inc() {};
    inline void code(MemCore& core, InstructionSet& iset) 
    { 
      n_ops++; 
      if ( !core.setF(core.getF()+1.0) )
        n_invops++;
    }
};

class Dec : public Instruction
{
  public:
    Dec() : Instruction() { name="dec"; DIS_flag = true; };
    ~Dec() {};
    inline void code(MemCore& core, InstructionSet& iset) 
    { 
      n_ops++;
      if ( !core.setF(core.getF()-1.0) )
        n_invops++;
    }
};

class Cmp : public Instruction
{
  public:
    Cmp() : Instruction() { name="cmp"; DIS_flag = true; };
    ~Cmp() {};
    inline void code(MemCore& core, InstructionSet& iset) 
    { 
      double retvalue=0;

      n_ops++;
      if (core.I<core.D_size) 
      {
        if (core.D_saved[core.I]) 
        {
          if ( core.getF() != core.D[core.I] )
            retvalue = -1;

          if ( !core.setF(retvalue) )
            n_invops++;
        }
        else
          n_invops++;
      }
      else
        n_invops++;
    }
};

class Load : public Instruction
{
  public:
    Load() : Instruction() { name="load"; DIS_flag = true; };
    ~Load() {};
    inline void code(MemCore& core, InstructionSet& iset) {
      n_ops++;
      if (core.I<core.D_size) 
      {
        if (core.D_saved[core.I]) 
        {
          if ( !core.setF(core.D[core.I]) )
            n_invops++;
        }
        else
          n_invops++;
      }
      else
        n_invops++;
    };
};

class Save : public Instruction
{
  public:
    Save() : Instruction() { name="save"; DIS_flag = true; };
    ~Save() {};
    inline void code(MemCore& core, InstructionSet& iset) {
      n_ops++;
      if (core.I<core.D_size) {
        core.D[core.I] = core.getF();
        core.D_saved[core.I] = true;
      }
      else
        n_invops++;
    }
};

class Swap : public Instruction
{
  public:
    Swap() : Instruction() { name="swap"; DIS_flag = true; };
    ~Swap() {};
    inline void code(MemCore& core, InstructionSet& iset) {
      n_ops++;
      if (core.I<core.D_size) {
        if (core.D_saved[core.I]) {
          double aux = core.D[core.I];
          core.D[core.I] = core.getF();
          core.setF(aux);
        }
        else
          n_invops++;
      }
      else
        n_invops++;
    }
};

class Label : public Instruction
{
  public:
    Label() : Instruction() { name="label"; DIS_flag = true; };
    ~Label() {};
    inline void code(MemCore& core, InstructionSet& iset) {
      n_ops++;
      if (core.I<core.L_size) {
        core.L[core.I] = core.c; // saves current position (next instruction will be executed when this label is called)
        core.L_saved[core.I] = true;
      }
      else
        n_invops++;
    }
};

class GotoIfP : public Instruction
{
  public:
    GotoIfP() : Instruction() { name="gotoifp"; DIS_flag = true; };
    ~GotoIfP() {};
    inline void code(MemCore& core, InstructionSet& iset) {
      n_ops++;
      if (core.I < core.L_size) {
        if (core.L_saved[core.I]) {
          if (core.getF()>=0) 
            core.c=core.L[core.I];
        }    
        else
          n_invops++;
      }
      else
        n_invops++;
    }
};

class JumpIfN : public Instruction
{
  private:
    bool never_called;
    std::vector<unsigned> J_table; // jump-table

    /* 
     * The present implementation of JumpIfN uses a "jump table". In Revision 1, upon every call to jumpifn
     * (if indeed F<0) the instruction implementation would search for the corresponding jumphere. This is
     * a very time-consuming task, especially when such jumps appear within long loops.
     *
     * With a jump-table formalism, we perform a one-time search for all the jumpifn's and their corresponding 
     * jumphere's, feeding that information to the jump-table. Later calls to jumpifn simply jump to the
     * corresponding point stored at the table, without any additional searches.
     *
     * Because JumpHere are dummy instructions, all of the implementation of JumpsIfN can be confined to here.
     *
     */
    
    inline void build_J_table(MemCore& core, InstructionSet& iset)
    {
      const unsigned C_size=(*core.C).size();
      unsigned curr_c=0; // starting from c=0 IS important! 
                         // other flow control instructions, including another jumpifn, might cause the first
                         // occurrence of jumpifn in the code to be bypassed
      unsigned searching_c;
      unsigned n_openjumps; // n_openjumps counts the number of jumps without a corresponding jumphere
      
      J_table.clear();
      
      for (unsigned i=0;i<C_size;i++)
        J_table.push_back(0); // zeroes table

      while (curr_c<C_size) // this loop searches for "jumpifn" instructions in the code
      {
        if (iset.getName((*core.C)[curr_c]) == "jumpifn") // if it's a jumpifn, searches for the corresponding jumphere
        {
          n_openjumps=1; // the current jumpifn is open
          searching_c=curr_c+1; // starts at the next instruction
          while ( (n_openjumps>0) && (searching_c<C_size) ) // searches for the corresponding jumphere
          {
            if (iset.getName((*core.C)[searching_c]) == "jumpifn") n_openjumps++;
            if (iset.getName((*core.C)[searching_c]) == "jumphere") n_openjumps--;
            searching_c++;
          }

          if (n_openjumps>0)
            J_table[curr_c]=0; // could not find a corresponding jumphere!
          else
            J_table[curr_c]=searching_c-1; // points to the jumphere instruction (interpreter will execute next one)
        } // if name is jumpifn
        curr_c++;
      }
      never_called=false;
    };

  public:
    void clear() { never_called = true; }
    JumpIfN() : Instruction() { name="jumpifn"; DIS_flag=true; clear(); };
    ~JumpIfN() {};
    inline void code(MemCore& core, InstructionSet& iset) 
    {
      n_ops++;
      if (core.getF()<0) 
      {
        if (never_called) 
          build_J_table(core, iset); // builds the jump-table on first call
        
        if (J_table[core.c]) // only jumps if a corresponding jumphere exists
          core.c = J_table[core.c];
        else
          n_invops++;
      }
    }
};

class JumpHere : public Instruction
{
  public:
    JumpHere() : Instruction() { name="jumphere"; DIS_flag = true; };
    ~JumpHere() {};
    inline void code(MemCore& core, InstructionSet& iset) { n_ops++; }
};

class Loop : public Instruction
{
    inline void build_L_table(MemCore& core, InstructionSet& iset)
    {
      const unsigned C_size=(*core.C).size();
      unsigned curr_c=0;
      unsigned searching_c;
      unsigned n_openloops; // counts the number of repeats without a corresponding endloop
      int depth, max_depth=0;
      
      core.L_table_addr.clear();
      core.L_table_count.clear();
      
      for (unsigned i=0;i<C_size;i++)
      {
        core.L_table_addr.push_back(0); // initializes and zeroes table
        core.L_table_count.push_back(0); // initializes and zeroes table
      }

      while (curr_c<C_size) // this loop searches for "loop" instructions in the code
      {
        if (iset.getName((*core.C)[curr_c]) == "loop") // if it's a "loop", searches for the corresponding endloop
        {
          depth=1;
          n_openloops=1; // the current loop is open
          searching_c=curr_c+1; // starts at the next instruction
          while ( (n_openloops>0) && (searching_c<C_size) ) // searches for the corresponding jumphere
          {
            if (iset.getName((*core.C)[searching_c]) == "loop") { n_openloops++; depth++; }
            if (iset.getName((*core.C)[searching_c]) == "endloop") n_openloops--;
            searching_c++;
          };

          if (n_openloops<=0) // has the corresponding "endloop" been found?
          {
            if (depth>max_depth)
              max_depth=depth;
            core.L_table_addr[curr_c]=searching_c-1;
            core.L_table_addr[searching_c-1]=curr_c; // points to the occurrence of "loop"
          }
        }
        curr_c++;
      } // end while

      if (iset.getMaxLoopDepth()>=0)
        if (max_depth>iset.getMaxLoopDepth())
          throw 0; // program has depth greater than allowed
    };

  public:
    Loop() : Instruction() { name="loop"; DIS_flag=true; };
    ~Loop() {};
    inline void code(MemCore& core, InstructionSet& iset) 
    {
      n_ops++;
      if (core.L_table_addr.size()==0)
        build_L_table(core, iset); // builds the loop-table on first call
      
      if (core.c < core.L_table_addr.size() && core.L_table_addr[core.c]) // does this loop instruction have a corresponding endloop?
      {
        if (core.I==0) // were we asked to jump the loop block?
          core.c=core.L_table_addr[core.c]; // points c to the corresponding endloop address (interpreter will execute the following instruction)
        else
          core.L_table_count[core.c]=core.I; // we're in a loop -- set the loop counter to core.I
      }
      else
        n_invops++; // could not find endloop for this loop!
    }
};

class EndLoop : public Instruction
{
  public:
    EndLoop() : Instruction() { name="endloop"; DIS_flag = true; };
    ~EndLoop() {};
    inline void code(MemCore& core, InstructionSet& iset) 
    {
      n_ops++;
      if (core.L_table_addr.size()>0) // does L_table_* exist?
      {
        if (core.c < core.L_table_addr.size() && core.L_table_addr[core.c]) // does this EndLoop have a corresponding Loop?
        {
          const unsigned loop_addr = core.L_table_addr[core.c];
          if (core.L_table_count[loop_addr]>1) // do we have any more loops left?
          {
            core.c = loop_addr; // points c to the corresponding "loop"
            core.L_table_count[loop_addr] -= 1; // decreases the loop counter
          }
        }
        else
          n_invops++;
      }
      else
        n_invops++;
    }
};

class Input : public Instruction
{
  public:
    Input() : Instruction() { name="input"; DIS_flag = true; };
    ~Input() {};
    inline void code(MemCore& core, InstructionSet& iset) {
      n_ops++;
      if ( (*core.input).size() == 0 ) 
      {
        double finput;
        std::cout << "Enter input #" << n_inputs+1 << ": ";
        std::cin >> finput;
        core.setF(finput);
      }
      else 
      {
        if ( n_inputs < (*core.input).size() )
          core.setF( (*core.input)[n_inputs] );
      }

      n_inputs++;
      if (!core.output_executed)
        n_inputs_bf_output++;
    }
};

class Output : public Instruction
{
  public:
    Output() : Instruction() { name="output"; DIS_flag = true; };
    ~Output() {};
    inline void code(MemCore& core, InstructionSet& iset) {
      n_ops++;
	  if ((*core.input).size() == 0)
	  {
		  std::cout << "Output #" << n_outputs + 1 << ": " << core.getF() << std::endl;
		  core.lOutput.push_back(core.getF());
	  }
	  else
	  {
		  (*core.output).push_back(core.getF());
		  core.lOutput.push_back(core.getF());
	  }

      n_outputs++;
      core.output_executed=true;
    }
};

class Abs : public Instruction
{
  public:
    Abs() : Instruction() { name="abs"; DIS_flag = true; };
    ~Abs() {};
    inline void code(MemCore& core, InstructionSet& iset) { core.setF( fabs(core.getF()) ); n_ops++; }
};

class Sign : public Instruction
{
  public:
    Sign() : Instruction() { name="sign"; DIS_flag = true; };
    ~Sign() {};
    inline void code(MemCore& core, InstructionSet& iset) { core.setF( -core.getF() );  n_ops++; }
};

class Exp : public Instruction
{
  public:
    Exp() : Instruction() { name="exp"; DIS_flag = true; };
    ~Exp() {};
    inline void code(MemCore& core, InstructionSet& iset) 
    { 
      n_ops++; 
      core.setF( exp(core.getF()) ); 
    }
};

class Log : public Instruction
{
  public:
    Log() : Instruction() { name="log"; DIS_flag = true; };
    ~Log() {};
    inline void code(MemCore& core, InstructionSet& iset) 
    { 
      n_ops++;
      if ( !core.setF( log(core.getF()) ) )
        n_invops++;
    }
};

class Sin : public Instruction
{
  public:
    Sin() : Instruction() { name="sin"; DIS_flag = true; };
    ~Sin() {};
    inline void code(MemCore& core, InstructionSet& iset) 
    { 
      if ( !core.setF(sin(core.getF())) )
        n_ops++; 
    }
};

class Cos : public Instruction
{
public:
    Cos() : Instruction() { name = "cos"; DIS_flag = true; };
    ~Cos() {};
    inline void code(MemCore& core, InstructionSet& iset)
    {
        if (!core.setF(cos(core.getF())))
            n_ops++;
    }
};

class Tan : public Instruction
{
public:
    Tan() : Instruction() { name = "tan"; DIS_flag = true; };
    ~Tan() {};
    inline void code(MemCore& core, InstructionSet& iset)
    {
        if (!core.setF(tan(core.getF())))
            n_ops++;
    }
};

class ATan : public Instruction
{
public:
    ATan() : Instruction() { name = "atan"; DIS_flag = true; };
    ~ATan() {};
    inline void code(MemCore& core, InstructionSet& iset)
    {
        if (!core.setF(atan(core.getF())))
            n_ops++;
    }
};

class TanH : public Instruction
{
public:
    TanH() : Instruction() { name = "tanh"; DIS_flag = true; };
    ~TanH() {};
    inline void code(MemCore& core, InstructionSet& iset)
    {
        if (!core.setF(tanh(core.getF())))
            n_ops++;
    }
};

class CosH : public Instruction
{
public:
    CosH() : Instruction() { name = "cosh"; DIS_flag = true; };
    ~CosH() {};
    inline void code(MemCore& core, InstructionSet& iset)
    {
        if (!core.setF(cosh(core.getF())))
            n_ops++;
    }
};

class Ceil : public Instruction
{
public:
    Ceil() : Instruction() { name = "ceil"; DIS_flag = true; };
    ~Ceil() {};
    inline void code(MemCore& core, InstructionSet& iset)
    {
        if (!core.setF(ceil(core.getF())))
            n_ops++;
    }
};

class Floor : public Instruction
{
public:
    Floor() : Instruction() { name = "floor"; DIS_flag = true; };
    ~Floor() {};
    inline void code(MemCore& core, InstructionSet& iset)
    {
        if (!core.setF(floor(core.getF())))
            n_ops++;
    }
};

class Sqrt : public Instruction
{
public:
    Sqrt() : Instruction() { name = "sqrt"; DIS_flag = true; };
    ~Sqrt() {};
    inline void code(MemCore& core, InstructionSet& iset)
    {
        if (!core.setF(sqrt(core.getF())))
            n_ops++;
    }
};

class Cbrt : public Instruction
{
public:
    Cbrt() : Instruction() { name = "cbrt"; DIS_flag = true; };
    ~Cbrt() {};
    inline void code(MemCore& core, InstructionSet& iset)
    {
        if (!core.setF(cbrt(core.getF())))
            n_ops++;
    }
};

class Log10 : public Instruction
{
public:
    Log10() : Instruction() { name = "log10"; DIS_flag = true; };
    ~Log10() {};
    inline void code(MemCore& core, InstructionSet& iset)
    {
        if (!core.setF(log10(core.getF())))
            n_ops++;
    }
};

class Erf : public Instruction
{
public:
    Erf() : Instruction() { name = "erf"; DIS_flag = true; };
    ~Erf() {};
    inline void code(MemCore& core, InstructionSet& iset)
    {
        if (!core.setF(erf(core.getF())))
            n_ops++;
    }
};

class ErfC : public Instruction
{
public:
    ErfC() : Instruction() { name = "erfc"; DIS_flag = true; };
    ~ErfC() {};
    inline void code(MemCore& core, InstructionSet& iset)
    {
        if (!core.setF(erfc(core.getF())))
            n_ops++;
    }
};

class LGamma : public Instruction
{
public:
    LGamma() : Instruction() { name = "lgamma"; DIS_flag = true; };
    ~LGamma() {};
    inline void code(MemCore& core, InstructionSet& iset)
    {
        if (!core.setF(lgamma(core.getF())))
            n_ops++;
    }
};

class TGamma : public Instruction
{
public:
    TGamma() : Instruction() { name = "tgamma"; DIS_flag = true; };
    ~TGamma() {};
    inline void code(MemCore& core, InstructionSet& iset)
    {
        if (!core.setF(tgamma(core.getF())))
            n_ops++;
    }
};

class ExpM1 : public Instruction
{
public:
    ExpM1() : Instruction() { name = "expm1"; DIS_flag = true; };
    ~ExpM1() {};
    inline void code(MemCore& core, InstructionSet& iset)
    {
        if (!core.setF(expm1(core.getF())))
            n_ops++;
    }
};

class IsFinite : public Instruction
{
public:
    IsFinite() : Instruction() { name = "isfinite"; DIS_flag = true; };
    ~IsFinite() {};
    inline void code(MemCore& core, InstructionSet& iset)
    {
        if (!core.setF(isfinite(core.getF())))
            n_ops++;
    }
};

class IsInfinite : public Instruction
{
public:
    IsInfinite() : Instruction() { name = "isinf"; DIS_flag = true; };
    ~IsInfinite() {};
    inline void code(MemCore& core, InstructionSet& iset)
    {
        if (!core.setF(isinf(core.getF())))
            n_ops++;
    }
};

class IsNan : public Instruction
{
public:
    IsNan() : Instruction() { name = "isnan"; DIS_flag = true; };
    ~IsNan() {};
    inline void code(MemCore& core, InstructionSet& iset)
    {
        if (!core.setF(isnan(core.getF())))
            n_ops++;
    }
};

class IsNormal : public Instruction
{
public:
    IsNormal() : Instruction() { name = "isnormal"; DIS_flag = true; };
    ~IsNormal() {};
    inline void code(MemCore& core, InstructionSet& iset)
    {
        if (!core.setF(isnormal(core.getF())))
            n_ops++;
    }
};

class Add : public Instruction
{
  public:
    Add() : Instruction() { name="add"; DIS_flag = true; };
    ~Add() {};
    inline void code(MemCore& core, InstructionSet& iset) 
    { 
      n_ops++;
      if (core.I < core.D_size) 
      {
        if ( core.D_saved[core.I] )
        {
          if ( !core.setF( core.getF()+core.D[core.I] ) )
            n_invops++;
        }
        else
          n_invops++;  // variable D[core.I] hasn't been saved
      }
      else
        n_invops++;  // variable D[core.I] is out of range        
    }
};

class Sub : public Instruction
{
  public:
    Sub() : Instruction() { name="sub"; DIS_flag = true; };
    ~Sub() {};
    inline void code(MemCore& core, InstructionSet& iset) {
      n_ops++;
      if (core.I < core.D_size) {
        if ( core.D_saved[core.I] )
        {
          if ( !core.setF( core.getF()-core.D[core.I] ) )
            n_invops++;
        }
        else
          n_invops++;  // variable D[core.I] hasn't been saved
      }
      else
        n_invops++;  // variable D[core.I] is out of range        
    }
};

class Mul : public Instruction
{
  public:
    Mul() : Instruction() { name="mul"; DIS_flag = true; };
    ~Mul() {};
    inline void code(MemCore& core, InstructionSet& iset) {
      n_ops++;
      if (core.I < core.D_size) {
        if ( core.D_saved[core.I] )
        {
          if ( !core.setF( core.getF()*core.D[core.I] ) )
            n_invops++;
        }
        else
          n_invops++;  // variable D[core.I] hasn't been saved
      }
      else
        n_invops++;  // variable D[core.I] is out of range        
    }
};

class Div : public Instruction
{
  public:
    Div() : Instruction() { name="div"; DIS_flag = true; };
    ~Div() {};
    inline void code(MemCore& core, InstructionSet& iset) {
      n_ops++;
      if (core.I < core.D_size) {
        if ( core.D_saved[core.I]  )
        {
          if ( !core.setF( core.getF()/core.D[core.I] ) )
            n_invops++;
        }
        else
          n_invops++;  // variable D[core.I] hasn't been saved
      }
      else
        n_invops++;  // variable D[core.I] is out of range
    }
};

class Pow : public Instruction
{
  public:
    Pow() : Instruction() { name="pow"; DIS_flag = true; };
    ~Pow() {};
    inline void code(MemCore& core, InstructionSet& iset) {
      n_ops++;
      if (core.I < core.D_size) {
        if ( core.D_saved[core.I] ) 
        {
          if ( !core.setF(pow(core.getF(),core.D[core.I])) )
            n_invops++;
        }
        else
          n_invops++;  // variable D[core.I] hasn't been saved
      }
      else
        n_invops++;  // variable D[core.I] is out of range
    }
};

class CheckCurrencyRequestValue : public Instruction
{
public:
    CheckCurrencyRequestValue() : Instruction() { name = "CheckCurrencyRequestValue"; DIS_flag = true; };
    ~CheckCurrencyRequestValue() {};
    inline void code(MemCore& core, InstructionSet& iset) { core.setF(WM::CheckCurrencyRequestValue(WM::GetMyId(), core.getF())); n_ops++; }
};

class CheckCurrencyRequestId : public Instruction
{
public:
    CheckCurrencyRequestId() : Instruction() { name = "CheckCurrencyRequestId"; DIS_flag = true; };
    ~CheckCurrencyRequestId() {};
    inline void code(MemCore& core, InstructionSet& iset) { core.setF(WM::CheckCurrencyRequestId(WM::GetMyId(), core.getF())); n_ops++; }
};

class RemoveCurrencyRequest : public Instruction
{
public:
    RemoveCurrencyRequest() : Instruction() { name = "RemoveCurrencyRequest"; DIS_flag = true; };
    ~RemoveCurrencyRequest() {};
    inline void code(MemCore& core, InstructionSet& iset) { WM::RemoveCurrencyRequest(WM::GetMyId(), core.getF()); n_ops++; }
};

class SendCurrencyRequest : public Instruction
{
public:
    SendCurrencyRequest() : Instruction() { name = "SendCurrencyRequest"; DIS_flag = true; };
    ~SendCurrencyRequest() {};
    inline void code(MemCore& core, InstructionSet& iset) {
        n_ops++;
        if (core.I < core.D_size) {
            if (core.D_saved[core.I])
            {
                WM::SendCurrencyRequest(WM::GetMyId(), core.getF(), core.D[core.I]);
            }
            else
                n_invops++;  // variable D[core.I] hasn't been saved
        }
        else
            n_invops++;  // variable D[core.I] is out of range
    }
};

class GetBalance : public Instruction
{
public:
    GetBalance() : Instruction() { name = "GetBalance"; DIS_flag = true; };
    ~GetBalance() {};
    inline void code(MemCore& core, InstructionSet& iset) { core.setF(WM::GetBalance(WM::GetMyId())); n_ops++; }
};

class SendCurrency : public Instruction
{
public:
    SendCurrency() : Instruction() { name = "SendCurrency"; DIS_flag = true; };
    ~SendCurrency() {};
    inline void code(MemCore& core, InstructionSet& iset) {
        n_ops++;
        if (core.I < core.D_size) {
            if (core.D_saved[core.I])
            {
                WM::SendCurrency(WM::GetMyId(), core.getF(), core.D[core.I]);
            }
            else
                n_invops++;  // variable D[core.I] hasn't been saved
        }
        else
            n_invops++;  // variable D[core.I] is out of range
    }
};

class GetId : public Instruction
{
public:
    GetId() : Instruction() { name = "GetId"; DIS_flag = true; };
    ~GetId() {};
    inline void code(MemCore& core, InstructionSet& iset) { core.setF(WM::GetId(core.getF())); n_ops++; }
};

class GetFitness : public Instruction
{
public:
    GetFitness() : Instruction() { name = "GetFitness"; DIS_flag = true; };
    ~GetFitness() {};
    inline void code(MemCore& core, InstructionSet& iset) { core.setF(WM::GetFitness(core.getF())); n_ops++; }
};

class GetGoal : public Instruction
{
public:
    GetGoal() : Instruction() { name = "GetGoal"; DIS_flag = true; };
    ~GetGoal() {};
    inline void code(MemCore& core, InstructionSet& iset) { core.setF(WM::GetGoal(core.getF())); n_ops++; }
};

class GetMyId : public Instruction
{
public:
    GetMyId() : Instruction() { name = "GetMyId"; DIS_flag = true; };
    ~GetMyId() {};
    inline void code(MemCore& core, InstructionSet& iset) { core.setF(WM::GetGoal(core.getF())); n_ops++; }
};

class ATan2 : public Instruction
{
public:
    ATan2() : Instruction() { name = "atan2"; DIS_flag = true; };
    ~ATan2() {};
    inline void code(MemCore& core, InstructionSet& iset) {
        n_ops++;
        if (core.I < core.D_size) {
            if (core.D_saved[core.I])
            {
                if (!core.setF(atan2(core.getF(), core.D[core.I])))
                    n_invops++;
            }
            else
                n_invops++;  // variable D[core.I] hasn't been saved
        }
        else
            n_invops++;  // variable D[core.I] is out of range
    }
};

class CopySign : public Instruction
{
public:
    CopySign() : Instruction() { name = "copysign"; DIS_flag = true; };
    ~CopySign() {};
    inline void code(MemCore& core, InstructionSet& iset) {
        n_ops++;
        if (core.I < core.D_size) {
            if (core.D_saved[core.I])
            {
                if (!core.setF(copysign(core.getF(), core.D[core.I])))
                    n_invops++;
            }
            else
                n_invops++;  // variable D[core.I] hasn't been saved
        }
        else
            n_invops++;  // variable D[core.I] is out of range
    }
};

class FDim : public Instruction
{
public:
    FDim() : Instruction() { name = "fdim"; DIS_flag = true; };
    ~FDim() {};
    inline void code(MemCore& core, InstructionSet& iset) {
        n_ops++;
        if (core.I < core.D_size) {
            if (core.D_saved[core.I])
            {
                if (!core.setF(fdim(core.getF(), core.D[core.I])))
                    n_invops++;
            }
            else
                n_invops++;  // variable D[core.I] hasn't been saved
        }
        else
            n_invops++;  // variable D[core.I] is out of range
    }
};

class FMod : public Instruction
{
public:
    FMod() : Instruction() { name = "fmod"; DIS_flag = true; };
    ~FMod() {};
    inline void code(MemCore& core, InstructionSet& iset) {
        n_ops++;
        if (core.I < core.D_size) {
            if (core.D_saved[core.I])
            {
                if (!core.setF(fmod(core.getF(), core.D[core.I])))
                    n_invops++;
            }
            else
                n_invops++;  // variable D[core.I] hasn't been saved
        }
        else
            n_invops++;  // variable D[core.I] is out of range
    }
};

class Ran : public Instruction
{
  public:
    Ran() : Instruction() { name="ran"; DIS_flag = true; };
    ~Ran() {};
    inline void code(MemCore& core, InstructionSet& iset) 
    {
      if ( !core.setF( NumericalRecipes::ran2(core.ran_ptr) ) )
        n_invops++;
      n_ops++;
    }
};

class Nop : public Instruction
{
  public:
    Nop() : Instruction() { name="nop"; DIS_flag = true; };
    ~Nop() {};
    inline void code(MemCore& core, InstructionSet& iset) { n_ops++; }
};

class PI : public Instruction
{
public:
    PI() : Instruction() { name = "pi"; DIS_flag = true; };
    ~PI() {};
    inline void code(MemCore& core, InstructionSet& iset)
    {
        if (!core.setF(M_PI))
            n_ops++;
    }
};

class E : public Instruction
{
public:
    E() : Instruction() { name = "e"; DIS_flag = true; };
    ~E() {};
    inline void code(MemCore& core, InstructionSet& iset)
    {
        if (!core.setF(M_PI))
            n_ops++;
    }
};

class Epsilon : public Instruction
{
public:
    Epsilon() : Instruction() { name = "epsilon"; DIS_flag = true; };
    ~Epsilon() {};
    inline void code(MemCore& core, InstructionSet& iset)
    {
        if (!core.setF(DBL_EPSILON))
            n_ops++;
    }
};

class MaxVal : public Instruction
{
public:
    MaxVal() : Instruction() { name = "maxval"; DIS_flag = true; };
    ~MaxVal() {};
    inline void code(MemCore& core, InstructionSet& iset)
    {
        if (!core.setF(DBL_MAX))
            n_ops++;
    }
};

class Zero : public Instruction
{
public:
    Zero() : Instruction() { name = "zero"; DIS_flag = true; };
    ~Zero() {};
    inline void code(MemCore& core, InstructionSet& iset)
    {
        if (!core.setF(0))
            n_ops++;
    }
};

class One : public Instruction
{
public:
    One() : Instruction() { name = "one"; DIS_flag = true; };
    ~One() {};
    inline void code(MemCore& core, InstructionSet& iset)
    {
        if (!core.setF(1))
            n_ops++;
    }
};

class C : public Instruction
{
public:
    C() : Instruction() { name = "c"; DIS_flag = true; };
    ~C() {};
    inline void code(MemCore& core, InstructionSet& iset)
    {
        if (!core.setF(2.99792458e8))
            n_ops++;
    }
};

class Mach : public Instruction
{
public:
    Mach() : Instruction() { name = "mach"; DIS_flag = true; };
    ~Mach() {};
    inline void code(MemCore& core, InstructionSet& iset)
    {
        if (!core.setF(331.46))
            n_ops++;
    }
};

class H : public Instruction
{
public:
    H() : Instruction() { name = "h"; DIS_flag = true; };
    ~H() {};
    inline void code(MemCore& core, InstructionSet& iset)
    {
        if (!core.setF(6.62606896e-34))
            n_ops++;
    }
};

class EqualTo : public Instruction
{
public:
    EqualTo() : Instruction() { name = "equalto"; DIS_flag = true; };
    ~EqualTo() {};
    inline void code(MemCore& core, InstructionSet& iset) {
        n_ops++;
        if (core.I < core.D_size) {
            if (core.D_saved[core.I])
            {
                if (!core.setF(core.getF() == core.D[core.I]))
                    n_invops++;
            }
            else
                n_invops++;  // variable D[core.I] hasn't been saved
        }
        else
            n_invops++;  // variable D[core.I] is out of range
    }
};

class NotEqualTo : public Instruction
{
public:
    NotEqualTo() : Instruction() { name = "notequalto"; DIS_flag = true; };
    ~NotEqualTo() {};
    inline void code(MemCore& core, InstructionSet& iset) {
        n_ops++;
        if (core.I < core.D_size) {
            if (core.D_saved[core.I])
            {
                if (!core.setF(core.getF() != core.D[core.I]))
                    n_invops++;
            }
            else
                n_invops++;  // variable D[core.I] hasn't been saved
        }
        else
            n_invops++;  // variable D[core.I] is out of range
    }
};

class GreaterThan : public Instruction
{
public:
    GreaterThan() : Instruction() { name = "greaterthan"; DIS_flag = true; };
    ~GreaterThan() {};
    inline void code(MemCore& core, InstructionSet& iset) {
        n_ops++;
        if (core.I < core.D_size) {
            if (core.D_saved[core.I])
            {
                if (!core.setF(core.getF() > core.D[core.I]))
                    n_invops++;
            }
            else
                n_invops++;  // variable D[core.I] hasn't been saved
        }
        else
            n_invops++;  // variable D[core.I] is out of range
    }
};

class LessThan : public Instruction
{
public:
    LessThan() : Instruction() { name = "lessthan"; DIS_flag = true; };
    ~LessThan() {};
    inline void code(MemCore& core, InstructionSet& iset) {
        n_ops++;
        if (core.I < core.D_size) {
            if (core.D_saved[core.I])
            {
                if (!core.setF(core.getF() < core.D[core.I]))
                    n_invops++;
            }
            else
                n_invops++;  // variable D[core.I] hasn't been saved
        }
        else
            n_invops++;  // variable D[core.I] is out of range
    }
};

class GreaterThanOrEqualTo : public Instruction
{
public:
    GreaterThanOrEqualTo() : Instruction() { name = "greaterthanorequalto"; DIS_flag = true; };
    ~GreaterThanOrEqualTo() {};
    inline void code(MemCore& core, InstructionSet& iset) {
        n_ops++;
        if (core.I < core.D_size) {
            if (core.D_saved[core.I])
            {
                if (!core.setF(core.getF() >= core.D[core.I]))
                    n_invops++;
            }
            else
                n_invops++;  // variable D[core.I] hasn't been saved
        }
        else
            n_invops++;  // variable D[core.I] is out of range
    }
};

class LessThanOrEqualTo : public Instruction
{
public:
    LessThanOrEqualTo() : Instruction() { name = "lessthanorequalto"; DIS_flag = true; };
    ~LessThanOrEqualTo() {};
    inline void code(MemCore& core, InstructionSet& iset) {
        n_ops++;
        if (core.I < core.D_size) {
            if (core.D_saved[core.I])
            {
                if (!core.setF(core.getF() <= core.D[core.I]))
                    n_invops++;
            }
            else
                n_invops++;  // variable D[core.I] hasn't been saved
        }
        else
            n_invops++;  // variable D[core.I] is out of range
    }
};

class And : public Instruction
{
public:
    And() : Instruction() { name = "and"; DIS_flag = true; };
    ~And() {};
    inline void code(MemCore& core, InstructionSet& iset) {
        n_ops++;
        if (core.I < core.D_size) {
            if (core.D_saved[core.I])
            {
                if (!core.setF(core.getF() && core.D[core.I]))
                    n_invops++;
            }
            else
                n_invops++;  // variable D[core.I] hasn't been saved
        }
        else
            n_invops++;  // variable D[core.I] is out of range
    }
};

class Or : public Instruction
{
public:
    Or() : Instruction() { name = "or"; DIS_flag = true; };
    ~Or() {};
    inline void code(MemCore& core, InstructionSet& iset) {
        n_ops++;
        if (core.I < core.D_size) {
            if (core.D_saved[core.I])
            {
                if (!core.setF(core.getF() || core.D[core.I]))
                    n_invops++;
            }
            else
                n_invops++;  // variable D[core.I] hasn't been saved
        }
        else
            n_invops++;  // variable D[core.I] is out of range
    }
};

class BitAnd : public Instruction
{
public:
    BitAnd() : Instruction() { name = "bitand"; DIS_flag = true; };
    ~BitAnd() {};
    inline void code(MemCore& core, InstructionSet& iset) {
        n_ops++;
        if (core.I < core.D_size) {
            if (core.D_saved[core.I])
            {
                if (!core.setF((int)core.getF() & (int)core.D[core.I]))
                    n_invops++;
            }
            else
                n_invops++;  // variable D[core.I] hasn't been saved
        }
        else
            n_invops++;  // variable D[core.I] is out of range
    }
};

class BitOr : public Instruction
{
public:
    BitOr() : Instruction() { name = "bitor"; DIS_flag = true; };
    ~BitOr() {};
    inline void code(MemCore& core, InstructionSet& iset) {
        n_ops++;
        if (core.I < core.D_size) {
            if (core.D_saved[core.I])
            {
                if (!core.setF((int)core.getF() | (int)core.D[core.I]))
                    n_invops++;
            }
            else
                n_invops++;  // variable D[core.I] hasn't been saved
        }
        else
            n_invops++;  // variable D[core.I] is out of range
    }
};

class XOr : public Instruction
{
public:
    XOr() : Instruction() { name = "xor"; DIS_flag = true; };
    ~XOr() {};
    inline void code(MemCore& core, InstructionSet& iset) {
        n_ops++;
        if (core.I < core.D_size) {
            if (core.D_saved[core.I])
            {
                if (!core.setF((int)core.getF() ^ (int)core.D[core.I]))
                    n_invops++;
            }
            else
                n_invops++;  // variable D[core.I] hasn't been saved
        }
        else
            n_invops++;  // variable D[core.I] is out of range
    }
};

class Not : public Instruction
{
public:
    Not() : Instruction() { name = "not"; DIS_flag = true; };
    ~Not() {};
    inline void code(MemCore& core, InstructionSet& iset)
    {
        if (!core.setF(~(int)core.getF()))
            n_ops++;
    }
};

class ShiftLeft : public Instruction
{
public:
    ShiftLeft() : Instruction() { name = "shiftleft"; DIS_flag = true; };
    ~ShiftLeft() {};
    inline void code(MemCore& core, InstructionSet& iset) {
        n_ops++;
        if (core.I < core.D_size) {
            if (core.D_saved[core.I])
            {
                if (!core.setF((int)core.getF() << (int)core.D[core.I]))
                    n_invops++;
            }
            else
                n_invops++;  // variable D[core.I] hasn't been saved
        }
        else
            n_invops++;  // variable D[core.I] is out of range
    }
};

class ShiftRight : public Instruction
{
public:
    ShiftRight() : Instruction() { name = "shiftright"; DIS_flag = true; };
    ~ShiftRight() {};
    inline void code(MemCore& core, InstructionSet& iset) {
        n_ops++;
        if (core.I < core.D_size) {
            if (core.D_saved[core.I])
            {
                if (!core.setF((int)core.getF() >> (int)core.D[core.I]))
                    n_invops++;
            }
            else
                n_invops++;  // variable D[core.I] hasn't been saved
        }
        else
            n_invops++;  // variable D[core.I] is out of range
    }
};

} // namespace DIS

} // namespace SlashA

