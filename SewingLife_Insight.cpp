// SewingLife_Insight.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

/*
 *
 *  slash - A command-line Slash/A interpreter illustrating the use of the Default Instruction Set (DIS).
 *
 *  Copyright (C) 2004-2011 Artur B Adib
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <algorithm>
#include <iostream>
#include <fstream>
#include <string>
#include <time.h>
#include <set>
#include "lib/SlashA.hpp"
#include <random>
#include <chrono>
#include "WorldManager.h"
#include <boost/functional/hash.hpp>
#include <unordered_map>
#include <boost/algorithm/sequence/edit_distance.hpp>

using namespace std;
using boost::algorithm::sequence::edit_distance;

static std::default_random_engine generator;

static void RandomProgram(SlashA::ByteCode& bc, SlashA::MemCore& memcore, int maxProgramLength, SlashA::InstructionSet& iset, int defaultInstructions, int outputs, int inputs, int mPercentage)
{
	// If we are replacing all the instructions, clear out the current instructions
	if (mPercentage == 100)
		bc.clear();

	memcore.lOutput.clear(); // Clear the output log

	int unique_instructions = iset.size() - defaultInstructions; // The default set only counts as 1 instruction

	std::set<int> ioLocations = std::set<int>(); // The locations where input/output instructions will be inserted

	SlashA::ByteCode_Type outputCode = SlashA::instruction2ByteCode("output", iset);
	SlashA::ByteCode_Type inputCode = SlashA::instruction2ByteCode("input", iset);

	// Keep trying to generate locations for output/input instructions until we get enough unique locations
	for (int i = 0; ioLocations.size() < outputs + inputs; i++)
	{
		ioLocations.emplace(rand() % maxProgramLength); // Insert an output/input instruction here
	}

	int ioInstructions = 0;
	auto curIOLocation = ioLocations.begin();

	for (int i = 0; i < maxProgramLength; i++)
	{
		if (curIOLocation != ioLocations.end() && i == *curIOLocation)
		{
			if (ioInstructions < outputs)
			{
				if (mPercentage == 100)
					bc.push_back(outputCode);
				else
					bc[i] = outputCode;
			}
			else
			{
				if (mPercentage == 100)
					bc.push_back(inputCode);
				else
					bc[i] = inputCode;
			}

			curIOLocation++;
			ioInstructions++;
		}
		else
		{
			int rInst = rand() % unique_instructions + 1;
			int instCode = defaultInstructions + rInst;

			// Ensure that we are not adding more output codes than we want
			while (instCode == outputCode || instCode == inputCode)
			{
				rInst = rand() % unique_instructions + 1;
				instCode = defaultInstructions + rInst;
			}

			if (rInst == unique_instructions)
				rInst = rand() % iset.size(); // Choose a random numeric value
			else
				rInst = instCode;

			if (mPercentage == 100)
				bc.push_back(rInst);
			else
			{
				bool replace = rand() % 100 < mPercentage;

				if (replace)
					bc[i] = rInst;
			}
		}
	}
}

static void RandomProgram(SlashA::ByteCode& bc, int maxProgramLength, SlashA::InstructionSet& iset, int defaultInstructions)
{
	bc.clear();

	int unique_instructions = iset.size() - defaultInstructions; // The default set only counts as 1 instruction

	int programLength = std::max(rand() % maxProgramLength, 3); // A program of at least 3 instructions

	for (int i = 0; i < programLength; i++)
	{
		int rInst = rand() % unique_instructions;

		if (rInst == 0)
			rInst = rand() % iset.size(); // Choose a random numeric value
		else
			rInst = defaultInstructions + rInst;

		bc.push_back(rInst);
	}

	bc.push_back(SlashA::instruction2ByteCode("output", iset));
}

static void CompleteRandomProgram(SlashA::ByteCode& bc, int maxProgramLength, SlashA::InstructionSet& iset, int defaultInstructions, 
	std::uniform_int_distribution<int> &instDist, std::uniform_int_distribution<int> &isetDist)
{
	bc.clear();

	for (int i = 0; i < maxProgramLength; i++)
	{
		int rInst = instDist(generator);

		if (rInst == 0)
			rInst = isetDist(generator); // Choose a random numeric value
		else
			rInst = defaultInstructions + rInst;

		bc.push_back(rInst);
	}
}

static void RandomProgramStructured(SlashA::ByteCode& bc, int maxProgramLength, SlashA::InstructionSet& iset, int defaultInstructions)
{
	bc.clear();

	int unique_instructions = iset.size() - defaultInstructions; // The default set only counts as 1 instruction

	SlashA::ByteCode_Type outputCode = SlashA::instruction2ByteCode("output", iset);
	SlashA::ByteCode_Type inputCode = SlashA::instruction2ByteCode("input", iset);

	bc.push_back(SlashA::instruction2ByteCode("input", iset));

	for (int i = 0; i < maxProgramLength; i++)
	{
		int rInst = rand() % unique_instructions + 1;
		int instCode = defaultInstructions + rInst;

		// Ensure that we are not adding more output codes than we want
		while (instCode == outputCode || instCode == inputCode)
		{
			rInst = rand() % unique_instructions + 1;
			instCode = defaultInstructions + rInst;
		}

		if (rInst == unique_instructions)
			rInst = rand() % iset.size(); // Choose a random numeric value
		else
			rInst = instCode;

		bc.push_back(rInst);
	}

	bc.push_back(SlashA::instruction2ByteCode("output", iset));
}

static void RandomProgramStrict(SlashA::ByteCode& bc, int programLength, SlashA::InstructionSet& iset, int defaultInstructions)
{
	bc.clear();

	int unique_instructions = iset.size() - defaultInstructions; // The default set only counts as 1 instruction

	std::uniform_int_distribution<int> instDist(0, unique_instructions);
	std::uniform_int_distribution<int> numDist(0, iset.size() - 1);

	SlashA::ByteCode_Type outputCode = SlashA::instruction2ByteCode("output", iset);
	SlashA::ByteCode_Type inputCode = SlashA::instruction2ByteCode("input", iset);

	bc.push_back(SlashA::instruction2ByteCode("input", iset));

	for (int i = 0; bc.size() < programLength; i++)
	{
		int rInst = instDist(generator);
		int instCode = defaultInstructions + rInst;

		// Ensure that we are not adding more output codes than we want
		while (instCode == outputCode || instCode == inputCode)
		{
			rInst = instDist(generator);
			instCode = defaultInstructions + rInst;
		}

		if (rInst == unique_instructions)
			rInst = numDist(generator); // Choose a random numeric value
		else
			rInst = instCode;

		bc.push_back(rInst);
	}

	bc.push_back(SlashA::instruction2ByteCode("output", iset));
}

static void RandomProgramNoI(SlashA::ByteCode& bc, int maxProgramLength, SlashA::InstructionSet& iset, int defaultInstructions)
{
	bc.clear();

	int unique_instructions = iset.size() - defaultInstructions; // The default set only counts as 1 instruction

	SlashA::ByteCode_Type outputCode = SlashA::instruction2ByteCode("output", iset);
	SlashA::ByteCode_Type inputCode = SlashA::instruction2ByteCode("input", iset);

	for (int i = 0; i < maxProgramLength; i++)
	{
		int rInst = rand() % unique_instructions + 1;
		int instCode = defaultInstructions + rInst;

		// Ensure that we are not adding more output codes than we want
		while (instCode == outputCode || instCode == inputCode)
		{
			rInst = rand() % unique_instructions + 1;
			instCode = defaultInstructions + rInst;
		}

		if (rInst == unique_instructions)
			rInst = rand() % iset.size(); // Choose a random numeric value
		else
			rInst = instCode;

		bc.push_back(rInst);
	}

	bc.push_back(SlashA::instruction2ByteCode("output", iset));
}

// Mutate any value within the program to any other value, except values that would break the programs structure
static void MutateProgram(SlashA::ByteCode& bc, int maxProgramLength, SlashA::InstructionSet& iset, int defaultInstructions, double mutationRate)
{
	int unique_instructions = iset.size() - defaultInstructions; // The default set only counts as 1 instruction

	SlashA::ByteCode_Type outputCode = SlashA::instruction2ByteCode("output", iset);
	SlashA::ByteCode_Type inputCode = SlashA::instruction2ByteCode("input", iset);

	// The mutation should not affect the first and last element of the program (input and output)
	for (int i = 1; i < bc.size() - 1; i++)
	{
		int mutateChance = rand() % 100;

		if (mutateChance <= 100 * mutationRate)
		{
			int rInst = rand() % unique_instructions + 1;
			int instCode = defaultInstructions + rInst;

			// Ensure that we are not adding more output codes than we want
			while (instCode == outputCode || instCode == inputCode)
			{
				rInst = rand() % unique_instructions + 1;
				instCode = defaultInstructions + rInst;
			}

			if (rInst == unique_instructions)
				rInst = rand() % iset.size(); // Choose a random numeric value
			else
				rInst = instCode;

			bc[i] = rInst;
		}
	}
}

// Mutate any value within the program to any other value
static void MutateUninhibited(SlashA::ByteCode& bc, SlashA::InstructionSet& iset, int defaultInstructions, double mutationRate)
{
	int unique_instructions = iset.size() - defaultInstructions; // The default set only counts as 1 instruction

	// The mutation should not affect the first and last element of the program (input and output)
	for (int i = 1; i < bc.size() - 1; i++)
	{
		int mutateChance = rand() % 10000;

		if (mutateChance <= 10000 * mutationRate)
		{
			int rInst = rand() % unique_instructions + 1;
			int instCode = defaultInstructions + rInst;

			if (rInst == unique_instructions)
				rInst = rand() % iset.size(); // Choose a random numeric value
			else
				rInst = instCode;

			bc[i] = rInst;
		}
	}
}

// Mutate any value within the program to any other value, replicate from the strongest neighbor with a probability based on the relative strength between the two programs
static void MutateAndReplicate(SlashA::ByteCode& bc, SlashA::ByteCode& neighbor, SlashA::InstructionSet& iset, int defaultInstructions, double mutationRate, double copyRate)
{
	int unique_instructions = iset.size() - defaultInstructions; // The default set only counts as 1 instruction

	std::uniform_int_distribution<int> instDist(0, unique_instructions);
	std::uniform_int_distribution<int> numDist(0, iset.size() - 1);

	// The mutation should not affect the first and last element of the program (input and output)
	for (int i = 0; i < bc.size() - 1; i++)
	{
		//int mutateChance = rand() % 10000;

		//if (mutateChance <= 10000 * mutationRate)
		//{
		//	int copyChance = rand() % 10000;

		//	if (copyChance <= 10000 * copyRate)
		//	{
		//		bc[i] = neighbor[i]; // Copy our neighbors value
		//	}
		//	else
		//	{
				int rInst = instDist(generator);
				int instCode = defaultInstructions + rInst;

				if (rInst == unique_instructions)
					rInst = numDist(generator); // Choose a random numeric value
				else
					rInst = instCode;

				bc[i] = rInst;
		//	}
		//}
	}
}

// Mutate any value within the program to any other value, replicate from the strongest neighbor with a probability based on the relative strength between the two programs
static void MutateSingle(SlashA::ByteCode& bc, SlashA::InstructionSet& iset, int defaultInstructions)
{
	int unique_instructions = iset.size() - defaultInstructions; // The default set only counts as 1 instruction
	int half_unique = (0.50 * unique_instructions);
	unique_instructions += half_unique;

	std::uniform_int_distribution<int> instDist(0, unique_instructions);
	std::uniform_int_distribution<int> numDist(0, iset.size() - 1);
	std::uniform_int_distribution<int> bcDist(0, bc.size() - 1);

	for (int i = 0; i < bc.size(); i++)
	{
		int rInst = instDist(generator);
		int instCode = defaultInstructions + rInst;

		if (rInst >= unique_instructions - half_unique)
			rInst = numDist(generator); // Choose a random numeric value
		else
			rInst = instCode;

		int target = bcDist(generator);
		bc[target] = rInst;
	}
}

// Test that we can randomly generate programs effectively
int test1()
{
	cout << "slash -- An interpreter for the Slash/A language" << endl;
	cout << SlashA::getHeader() << endl << endl;

	srand(time(NULL)); // Initialize rand, should use the other rand for this

	for (int i = 0; i < 100; i++)
	{
		try
		{
			const unsigned DEFAULT_SET = 32768;
			vector<double> input, output;
			SlashA::ByteCode bc;
			SlashA::InstructionSet iset(DEFAULT_SET); // initializes the Default Instruction Set with 32768 numeric instructions
			SlashA::MemCore memcore(10, // length of the data tape D[]
				10, // length of the label tape L[]
				input, // input buffer (will use keyboard if empty)
				output); // output buffer

			iset.insert_DIS_full();

			int maxProgramLength = 32; // The maximum number of instructions a program can have.

			RandomProgram(bc, memcore, maxProgramLength, iset, DEFAULT_SET, 4, 0, 100); // Generate a random program with a "fixed" number of inputs/outputs
			// RandomProgram(bc, maxProgramLength, iset, DEFAULT_SET); // Generate a random program with any number of inputs/outputs

			bool failed = SlashA::runByteCode(iset, // instruction set pointer
				memcore, // memory core pointer
				bc, // ByteCoded program to be run (pointer)
				-2237, // random seed for random number instructions
				0, // max run time in seconds (0 for no limit)
				-1); // max loop depth

			if (failed)
				cout << "Program failed (time-out, loop depth, etc)!" << endl;

			cout << endl;
			cout << "Total number of operations: " << iset.getTotalOps() << endl;
			cout << "Total number of invalid operations: " << iset.getTotalInvops() << endl;
			cout << "Total number of inputs before an output: " << iset.getTotalInputsBFOutput() << endl;
		}
		catch (string& s)
		{
			cout << s << endl << endl;
			continue;
		}

		cout << endl;
	}

	return 0;
}

static vector<double> Evaluate(SlashA::ByteCode& bc, SlashA::MemCore& memcore, SlashA::InstructionSet& iset, int time = 1)
{
	SlashA::runByteCode(iset, // instruction set pointer
		memcore, // memory core pointer
		bc, // ByteCoded program to be run (pointer)
		-2237, // random seed for random number instructions
		time, // max run time in seconds (0 for no limit)
		-1); // max loop depth

	return memcore.lOutput;
}

bool Replicate(SlashA::ByteCode& bc, SlashA::MemCore& memcore, SlashA::InstructionSet& iset, double target, int& copied)
{
	SlashA::ByteCode nc;

	// The application must output something
	vector<double> fitnessOutput = Evaluate(bc, memcore, iset);

	if (fitnessOutput.size() == 0)
		return false;

	// Successfully copy the input program
	for (int i = 0; i < bc.size(); i++, copied++)
	{
		unsigned int nInst = Evaluate(bc, memcore, iset)[0];

		if (nInst != bc[i])
			return false;

		nc.push_back(nInst);
	}

	// How many times will we try to modify a program before we give up?
	const int TIMEOUT = 100;
	for (int j = 0; j < TIMEOUT; j++)
	{
		bool modified = false;
		// Successfully modify the new program
		for (int i = 0; i < bc.size(); i++)
		{
			memcore.input->push_back(i);
			int nInst = Evaluate(bc, memcore, iset)[0];

			if (nInst != bc[i])
				modified = true;

			nc[i] = nInst;
		}

		if (!modified)
			return false;

		vector<double> input, output;
		SlashA::MemCore nemcore(10, // length of the data tape D[]
			10, // length of the label tape L[]
			input, // input buffer (will use keyboard if empty)
			output); // output buffer

		// The application must output something
		vector<double> nFitnessOutput = Evaluate(nc, nemcore, iset);

		if (nFitnessOutput.size() == 0)
			continue;

		if (abs(nFitnessOutput[0] - target) >= abs(fitnessOutput[0] - target))
			continue;

		// Replace the old program with the new program
		for (int i = 0; i < nc.size(); i++)
		{
			bc[i] = nc[i];
		}

		return true;
	}

	return false;
}

bool ForceReplicate(SlashA::ByteCode& bc, SlashA::MemCore& memcore, SlashA::InstructionSet& iset, double target)
{
	SlashA::ByteCode nc;

	// The application must output something
	vector<double> fitnessOutput = Evaluate(bc, memcore, iset);

	if (fitnessOutput.size() == 0)
		return false;

	nc = bc;

	for (int i = 0; i < bc.size(); i++)
	{
		nc.push_back(bc[i]);
	}

	// How many times will we try to modify a program before we give up?
	const int TIMEOUT = 100;
	for (int j = 0; j < TIMEOUT; j++)
	{
		bool modified = false;
		// Successfully modify the new program
		for (int i = 0; i < bc.size(); i++)
		{
			memcore.input->push_back(i);
			int nInst = Evaluate(bc, memcore, iset)[0];

			if (nInst != bc[i])
				modified = true;

			nc[i] = nInst;
		}

		if (!modified)
			return false;

		vector<double> input, output;
		SlashA::MemCore nemcore(10, // length of the data tape D[]
			10, // length of the label tape L[]
			input, // input buffer (will use keyboard if empty)
			output); // output buffer

		// The application must output something
		vector<double> nFitnessOutput = Evaluate(nc, nemcore, iset);

		if (nFitnessOutput.size() == 0)
			continue;

		if (abs(nFitnessOutput[0] - target) >= abs(fitnessOutput[0] - target))
			continue;

		// Replace the old program with the new program
		for (int i = 0; i < nc.size(); i++)
		{
			bc[i] = nc[i];
		}

		return true;
	}

	return false;
}

// Test that we can create programs that are evaluated on the basis of fitness
int test2()
{
	cout << "slash -- An interpreter for the Slash/A language" << endl;
	cout << SlashA::getHeader() << endl << endl;

	srand(time(NULL)); // Initialize rand, should use the other rand for this

	try
	{
		const unsigned DEFAULT_SET = 32768;
		vector<double> input, output;
		SlashA::ByteCode bc;
		SlashA::InstructionSet iset(DEFAULT_SET); // initializes the Default Instruction Set with 32768 numeric instructions
		SlashA::MemCore memcore(10, // length of the data tape D[]
			10, // length of the label tape L[]
			input, // input buffer (will use keyboard if empty)
			output); // output buffer

		iset.insert_DIS_full();

		int maxProgramLength = 50; // The maximum number of instructions a program can have.
		int generations = 100000;

		int successfulOutputs = 0;
		RandomProgram(bc, memcore, maxProgramLength, iset, DEFAULT_SET, 1, 0, 100); // Generate a random program with a "fixed" number of inputs/outputs

		SlashA::ByteCode baseProgram = bc;

		// Have we been successful creating a single self replicating, self modifying organism?
		bool success = false;
		for (int i = 0; i < generations; i++)
		{
			cout << endl << "Generation " << i << endl << endl;

			int successfullyCopiedValues = 0;

			if (!success)
			{
				memcore.lOutput.clear();
				bc = baseProgram;
				RandomProgram(bc, memcore, maxProgramLength, iset, DEFAULT_SET, 1, 0, 70); // Generate a random program with a "fixed" number of inputs/outputs

				//success = Replicate(bc, memcore, iset, 3723.38282, successfullyCopiedValues);

				//if (successfullyCopiedValues > successfulOutputs)
				//{
				//	successfulOutputs = successfullyCopiedValues;
				//	baseProgram = bc;
				//}

				success = ForceReplicate(bc, memcore, iset, 3723.38282);

				if (success)
					cout << "A self replicator appears!!!!!!!!!!!!!!" << endl;
			}
			else
			{
				Replicate(bc, memcore, iset, 3723.38282, successfullyCopiedValues);
			}
		}

		cout << endl;
		cout << "Total number of operations: " << iset.getTotalOps() << endl;
		cout << "Total number of invalid operations: " << iset.getTotalInvops() << endl;
		cout << "Total number of inputs before an output: " << iset.getTotalInputsBFOutput() << endl;
	}
	catch (string& s)
	{
		cout << s << endl << endl;
		exit(1);
	}

	cout << endl;

	return 0;
}

// Test that we can randomly generate programs effectively
int test3()
{
	cout << "slash -- An interpreter for the Slash/A language, test 3" << endl;
	cout << SlashA::getHeader() << endl << endl;

	srand(time(NULL)); // Initialize rand, should use the other rand for this

	double desiredOutput = 1538200000000000000;
	double smallestDelta = DBL_MAX;
	SlashA::ByteCode winningCodeA;
	bool initialSetComplete = false;

	while (!initialSetComplete)
	{
		try
		{
			const unsigned DEFAULT_SET = 32768;
			vector<double> input, output;
			SlashA::ByteCode bc;
			SlashA::InstructionSet iset(DEFAULT_SET); // initializes the Default Instruction Set with 32768 numeric instructions
			SlashA::MemCore memcore(10, // length of the data tape D[]
				10, // length of the label tape L[]
				input, // input buffer (will use keyboard if empty)
				output); // output buffer

			iset.insert_DIS_full_minus_Gotos_Loops();

			int maxProgramLength = 10000; // The maximum number of instructions a program can have.

			RandomProgramStructured(bc, maxProgramLength, iset, DEFAULT_SET); // Generate a random program with a "fixed" number of inputs/outputs

			memcore.input->push_back(1); // Our input, its arbitrary for the moment
			vector<double> eOutput = Evaluate(bc, memcore, iset);

			if (eOutput.empty())
				continue;

			if (abs(eOutput[0] - desiredOutput) < smallestDelta)
			{
				smallestDelta = abs(eOutput[0] - desiredOutput);
				winningCodeA = bc;
				initialSetComplete = true;
			}

			cout << "Current smallest delta is: " << smallestDelta << endl;
			cout << "Total number of operations: " << iset.getTotalOps() << endl;
			cout << "Total number of invalid operations: " << iset.getTotalInvops() << endl;
			cout << "Total number of inputs before an output: " << iset.getTotalInputsBFOutput() << endl;
		}
		catch (int e)
		{
			continue;
		}

		cout << endl;
	}

	bool mutate = true;
	for (int r = 0; r < 1000; r++)
	{
		cout << "BEGIN PART " << r + 1 << endl;

		double initialFitness = smallestDelta;
		SlashA::ByteCode winningCodeB = winningCodeA;
		for (int i = 0; i < 10; i++)
		{
			try
			{
				const unsigned DEFAULT_SET = 32768;
				vector<double> input, output;
				SlashA::ByteCode bc;
				SlashA::InstructionSet iset(DEFAULT_SET); // initializes the Default Instruction Set with 32768 numeric instructions
				SlashA::MemCore memcore(10, // length of the data tape D[]
					10, // length of the label tape L[]
					input, // input buffer (will use keyboard if empty)
					output); // output buffer

				iset.insert_DIS_full_minus_Gotos_Loops();

				int maxProgramLength = 32; // The maximum number of instructions a program can have.

				memcore.input->push_back(1); // Our input, its arbitrary for the moment

				if (mutate)
					MutateProgram(winningCodeA, maxProgramLength, iset, DEFAULT_SET, 0.001);
				else
					RandomProgramNoI(bc, maxProgramLength, iset, DEFAULT_SET); // Generate a random program with a "fixed" number of inputs/outputs

				SlashA::ByteCode winningCopy = winningCodeA;

				if (!mutate)
				{
					winningCopy.pop_back();
					winningCopy.insert(winningCopy.end(), bc.begin(), bc.end());
				}

				vector<double> eOutput = Evaluate(winningCopy, memcore, iset);

				if (eOutput.empty())
					continue;

				if (abs(eOutput[0] - desiredOutput) < smallestDelta)
				{
					smallestDelta = abs(eOutput[0] - desiredOutput);
					winningCodeB = winningCopy;
				}

				cout << r << " " << i << " Current smallest delta is: " << smallestDelta << endl;
				cout << "Total number of operations: " << iset.getTotalOps() << endl;
				cout << "Total number of invalid operations: " << iset.getTotalInvops() << endl;
				cout << "Total number of inputs before an output: " << iset.getTotalInputsBFOutput() << endl;
			}
			catch (int e)
			{
				continue;
			}

			cout << endl;
		}

		winningCodeA = winningCodeB;

		//if (smallestDelta == initialFitness)
		//	mutate = !mutate;
		//else
		//	mutate = true;
	}

	cout << "The smallest delta is: " << smallestDelta;

	return 0;
}

// Obtain the median result at the desired scale
double getSubMedian(SlashA::ByteCode entire_program, int subset_size, int sample_count)
{
	const unsigned DEFAULT_SET = 32768;
	SlashA::ByteCode subprogram;

	double average = 0.0;
	// Sample the lowest scale possible.
	for (int i = 0; i < sample_count; i++)
	{
		vector<double> input, output;
		SlashA::MemCore memcore(10, // length of the data tape D[]
			10, // length of the label tape L[]
			input, // input buffer (will use keyboard if empty)
			output); // output buffer

		memcore.input->push_back(1); // Our input, its arbitrary for the moment

		SlashA::InstructionSet iset(DEFAULT_SET); // initializes the Default Instruction Set with 32768 numeric instructions
		iset.insert_DIS_full_minus_Gotos_Loops();
		SlashA::ByteCode_Type outputCode = SlashA::instruction2ByteCode("output", iset);

		// We should not be able to use instructions multiple times
		SlashA::ByteCode availableInstructions = entire_program;
		subprogram = SlashA::ByteCode();

		for (int j = 0; j < subset_size; j++)
		{
			int selectedInst = rand() % availableInstructions.size();
			subprogram.push_back(availableInstructions[selectedInst]);
			availableInstructions.erase(availableInstructions.begin() + selectedInst); // Remove used instructions
		}

		// We need this to ensure that we have some output value
		// TODO: Ensure that we remove all output instructions before calling this function or at the beginning of the function
		subprogram.push_back(outputCode);

		vector<double> eOutput = Evaluate(subprogram, memcore, iset);

		if (eOutput.empty())
			average += 0.0; // We will treat this as the worst possible value, as it should be the farthest value from our goal
		else
			average += eOutput[0];
	}

	double meanValue = average / sample_count;
	return abs(meanValue);
}

// Determine how close the results at each are following our desired distribution (currently the exponential distribution)
double multiScaleMedian(SlashA::ByteCode entire_program, double desired_output, double base_error, int sample_count, int subsample_count)
{
	// We store the median values to determine median error from the model for all the scales
	double result = 0.0;
	// The number of scales that we would like to evaluate
	for (int i = 0; i < sample_count; i++)
	{
		int selectedScale = rand() % entire_program.size();

		double sBaseValue = getSubMedian(entire_program, selectedScale, subsample_count);
		double sBaseError = abs(sBaseValue - desired_output); // The error at the current scale
		// This should be the difference between the actual value and the expected value
		// In this case we're assuming that the error is decreasing at an exponential rate as scale increases
		double sMetaBaseError = abs(sBaseError - pow(base_error, -selectedScale));
		result += sMetaBaseError;
	}

	double meanValue = result / sample_count;
	return meanValue;
}

// Test multi-scale curve based optimization, exponential test
int test4()
{
	cout << "slash -- An interpreter for the Slash/A language, test 4" << endl;
	cout << SlashA::getHeader() << endl << endl;

	srand(time(NULL)); // Initialize rand, should use the other rand for this

	double desiredOutput = 37283;
	double smallestDelta = DBL_MAX;
	SlashA::ByteCode winningCodeA;
	bool initialSetComplete = false;

	while (!initialSetComplete)
	{
		try
		{
			const unsigned DEFAULT_SET = 32768;
			vector<double> input, output;
			SlashA::ByteCode bc;
			SlashA::InstructionSet iset(DEFAULT_SET); // initializes the Default Instruction Set with 32768 numeric instructions
			SlashA::MemCore memcore(10, // length of the data tape D[]
				10, // length of the label tape L[]
				input, // input buffer (will use keyboard if empty)
				output); // output buffer

			iset.insert_DIS_full_minus_Gotos_Loops();

			int maxProgramLength = 200; // The maximum number of instructions a program can have.

			RandomProgramStructured(bc, maxProgramLength, iset, DEFAULT_SET); // Generate a random program with a "fixed" number of inputs/outputs

			memcore.input->push_back(1); // Our input, its arbitrary for the moment
			vector<double> eOutput = Evaluate(bc, memcore, iset);

			if (eOutput.empty())
				continue;

			if (abs(eOutput[0] - desiredOutput) < smallestDelta)
			{
				smallestDelta = abs(eOutput[0] - desiredOutput);
				winningCodeA = bc;
				initialSetComplete = true;
			}

			cout << "Current smallest delta is: " << smallestDelta << endl;
			cout << "Total number of operations: " << iset.getTotalOps() << endl;
			cout << "Total number of invalid operations: " << iset.getTotalInvops() << endl;
			cout << "Total number of inputs before an output: " << iset.getTotalInputsBFOutput() << endl;
		}
		catch (int e)
		{
			continue;
		}

		cout << endl;
	}

	double finalError = DBL_MAX;
	for (int r = 0; r < 1000; r++)
	{
		cout << "BEGIN PART " << r + 1 << endl;

		SlashA::ByteCode winningCodeB = winningCodeA;

		// How far is the deviation from our model
		// This value should be getting smaller as the program improves implying that the program will get better
		// as we scale it. We scale the program by duplicating the largest layer or some sublayer, whichever gets us closer to
		// our desired size, whether that desired size is determined by memory, cpu, or some other limited resource
		double totalError = multiScaleMedian(winningCodeB, desiredOutput, desiredOutput, 10, 10);
		finalError = totalError;

		cout << "Total error: " << totalError << endl;

		// The number of scales that we will be sampling each run.
		for (int i = 0; i < 10; i++)
		{
			try
			{
				const unsigned DEFAULT_SET = 32768;
				vector<double> input, output;
				SlashA::ByteCode bc;
				SlashA::InstructionSet iset(DEFAULT_SET); // initializes the Default Instruction Set with 32768 numeric instructions
				SlashA::MemCore memcore(10, // length of the data tape D[]
					10, // length of the label tape L[]
					input, // input buffer (will use keyboard if empty)
					output); // output buffer

				iset.insert_DIS_full_minus_Gotos_Loops();

				memcore.input->push_back(1); // Our input, its arbitrary for the moment

				SlashA::ByteCode winningCopy = winningCodeB;

				MutateProgram(winningCopy, 0, iset, DEFAULT_SET, 0.50);

				// How far is the deviation from our model
				// This value should be getting smaller as the program improves implying that the program will get better
				// as we scale it. We scale the program by duplicating the largest layer or some sublayer, whichever gets us closer to
				// our desired size, whether that desired size is determined by memory, cpu, or some other limited resource
				double mTotalError = multiScaleMedian(winningCopy, desiredOutput, desiredOutput, 10, 10);

				// Keep track of how small the actual difference is between the actual result and desired result
				// Keep in mind that the best scaling program may not produce the smallest error at its current size
				vector<double> eOutput = Evaluate(winningCodeB, memcore, iset);

				bool hasTotalErrorReduced = mTotalError < totalError;
				bool hasTotalErrorHeld = mTotalError == totalError;
				bool hasResultErrorReduced = abs(eOutput[0] - desiredOutput) < smallestDelta;

				if (hasTotalErrorReduced || (hasTotalErrorHeld && hasResultErrorReduced))
				{
					totalError = mTotalError;
					winningCodeB = winningCopy;
					finalError = totalError;

					if (!eOutput.empty())
						smallestDelta = abs(eOutput[0] - desiredOutput);
				}

				cout << "The winners best delta from our desired value is: " << smallestDelta << endl;
				cout << r << " " << i << " Current total error is: " << totalError << endl;
				cout << "Total number of operations: " << iset.getTotalOps() << endl;
				cout << "Total number of invalid operations: " << iset.getTotalInvops() << endl;
				cout << "Total number of inputs before an output: " << iset.getTotalInputsBFOutput() << endl;
			}
			catch (int e)
			{
				continue;
			}

			cout << endl;
		}

		winningCodeA = winningCodeB;
	}

	cout << "The winner's improvement deviates from our desired distribution by: " << finalError;
	cout << "The winners best delta from our desired value is: " << smallestDelta;

	return 0;
}

int test5()
{
	cout << "slash -- An interpreter for the Slash/A language, test 5" << endl;
	cout << SlashA::getHeader() << endl << endl;

	srand(time(NULL)); // Initialize rand, should use the other rand for this

	double desiredOutput = 37283;
	double smallestDelta = DBL_MAX;
	SlashA::ByteCode winningCodeA;
	bool initialSetComplete = false;

	while (!initialSetComplete)
	{
		try
		{
			const unsigned DEFAULT_SET = 32768;
			vector<double> input, output;
			SlashA::ByteCode bc;
			SlashA::InstructionSet iset(DEFAULT_SET); // initializes the Default Instruction Set with 32768 numeric instructions
			SlashA::MemCore memcore(10, // length of the data tape D[]
				10, // length of the label tape L[]
				input, // input buffer (will use keyboard if empty)
				output); // output buffer

			iset.insert_DIS_full_minus_Gotos_Loops();

			int maxProgramLength = 200; // The maximum number of instructions a program can have.

			RandomProgramStructured(bc, maxProgramLength, iset, DEFAULT_SET); // Generate a random program with a "fixed" number of inputs/outputs

			memcore.input->push_back(1); // Our input, its arbitrary for the moment
			vector<double> eOutput = Evaluate(bc, memcore, iset);

			if (eOutput.empty())
				continue;

			if (abs(eOutput[0] - desiredOutput) < smallestDelta)
			{
				smallestDelta = abs(eOutput[0] - desiredOutput);
				winningCodeA = bc;
				initialSetComplete = true;
			}

			cout << "Current smallest delta is: " << smallestDelta << endl;
			cout << "Total number of operations: " << iset.getTotalOps() << endl;
			cout << "Total number of invalid operations: " << iset.getTotalInvops() << endl;
			cout << "Total number of inputs before an output: " << iset.getTotalInputsBFOutput() << endl;
		}
		catch (int e)
		{
			continue;
		}

		cout << endl;
	}

	double finalError = DBL_MAX;
	for (int r = 0; r < 1000; r++)
	{
		cout << "BEGIN PART " << r + 1 << endl;

		SlashA::ByteCode winningCodeB = winningCodeA;

		int selectedScale = rand() % winningCodeB.size();

		if (selectedScale == 0)
			continue;

		//double expectedError = pow(desiredOutput, -selectedScale);
		// How far is the deviation from our model
		// This value should be getting smaller as the program improves implying that the program will get better
		// as we scale it. We scale the program by duplicating the largest layer or some sublayer, whichever gets us closer to
		// our desired size, whether that desired size is determined by memory, cpu, or some other limited resource
		//double totalError = abs(getSubMedian(winningCodeB, selectedScale, 10) - expectedError);
		double totalError = abs(multiScaleMedian(winningCodeB, desiredOutput, desiredOutput, 10, 10));

		cout << "Total error: " << totalError << endl;

		// The number of scales that we will be sampling each run.
		for (int i = 0; i < 10; i++)
		{
			try
			{
				const unsigned DEFAULT_SET = 32768;
				vector<double> input, output;
				SlashA::ByteCode bc;
				SlashA::InstructionSet iset(DEFAULT_SET); // initializes the Default Instruction Set with 32768 numeric instructions
				SlashA::MemCore memcore(10, // length of the data tape D[]
					10, // length of the label tape L[]
					input, // input buffer (will use keyboard if empty)
					output); // output buffer

				iset.insert_DIS_full_minus_Gotos_Loops();

				memcore.input->push_back(1); // Our input, its arbitrary for the moment

				SlashA::ByteCode winningCopy = winningCodeB;

				MutateProgram(winningCopy, 0, iset, DEFAULT_SET, 0.05);

				//double expectedError = pow(desiredOutput, -selectedScale);
				// How far is the deviation from our model
				// This value should be getting smaller as the program improves implying that the program will get better
				// as we scale it. We scale the program by duplicating the largest layer or some sublayer, whichever gets us closer to
				// our desired size, whether that desired size is determined by memory, cpu, or some other limited resource
				double mTotalError = abs(multiScaleMedian(winningCopy, desiredOutput, desiredOutput, 10, 10));

				// Keep track of how small the actual difference is between the actual result and desired result
				// Keep in mind that the best scaling program may not produce the smallest error at its current size
				vector<double> eOutput = Evaluate(winningCopy, memcore, iset);

				double absError = abs(eOutput[0] - desiredOutput);
				bool hasTotalErrorReduced = mTotalError < totalError;
				bool hasTotalErrorHeld = mTotalError == totalError;
				bool hasResultErrorReduced = absError < smallestDelta;

				if (hasTotalErrorReduced || (hasTotalErrorHeld && hasResultErrorReduced))
				{
					totalError = mTotalError;
					winningCodeB = winningCopy;
					finalError = absError;

					if (!eOutput.empty())
						smallestDelta = abs(eOutput[0] - desiredOutput);
				}

				cout << "The winners best delta from our desired value is: " << smallestDelta << endl;
				cout << r << " " << i << " Current total error is: " << totalError << endl;
				cout << r << " " << i << " Current abs error is: " << absError << endl;
				cout << "Total number of operations: " << iset.getTotalOps() << endl;
				cout << "Total number of invalid operations: " << iset.getTotalInvops() << endl;
				cout << "Total number of inputs before an output: " << iset.getTotalInputsBFOutput() << endl;
				cout << "The scale is: " << selectedScale << endl;
			}
			catch (int e)
			{
				continue;
			}

			cout << endl;
		}

		winningCodeA = winningCodeB;
	}

	cout << "The winner's improvement deviates from our desired distribution by: " << finalError;
	cout << "The winners best delta from our desired value is: " << smallestDelta;

	return 0;
}

static double CalculateCopyRate(double curFitness, double StrongestNeighborFitness)
{
	if (curFitness == StrongestNeighborFitness)
		return 1;

	if (curFitness == 0 || StrongestNeighborFitness == 0 || isinf(curFitness) || isinf(StrongestNeighborFitness))
		return 0;

	double processRate = 0;

	if (StrongestNeighborFitness < curFitness)
		processRate = 100 - (StrongestNeighborFitness / curFitness); // Or should this be 100?
	else
		processRate = curFitness / StrongestNeighborFitness;

	return processRate;
}

static bool skipGeneration(double layerFitnessDelta, double totalFitnessDelta)
{
	if (layerFitnessDelta == 0 || totalFitnessDelta == 0 || !isfinite(layerFitnessDelta) || !isfinite(totalFitnessDelta))
		return false;

	double processRate = 0;

	if (totalFitnessDelta < 0 && layerFitnessDelta < 0)
	{
		if (totalFitnessDelta < layerFitnessDelta)
			processRate = 1.0 - (layerFitnessDelta / totalFitnessDelta);
		else
			processRate = totalFitnessDelta / layerFitnessDelta;
	}
	else if (totalFitnessDelta < 0 && layerFitnessDelta > 0)
		processRate = 1.0 + (totalFitnessDelta / layerFitnessDelta);
	else
		processRate = layerFitnessDelta / totalFitnessDelta;

	std::uniform_int_distribution<int> processDist(0, 10000);

	int processChance = processDist(generator);

	if (processChance <= 10000 * processRate)
		return false;
	else
		return true;
}

struct uProgram {
	int start, end; // The indexes we use to determine what part of the "total" bc is this individual program
	double parentFitness; // We are attempting to minimize this value
	double selfFitness;
	double fitnessDelta;
	double mutationRate;
	int parent;
	SlashA::ByteCode prevProgram;
	std::string individualGoal;
	int failCount;
};

struct wProgram {
	double worstFitness = -DBL_MAX; // We start off at the best fitness
	int worstProgram = -1;
	int worstLayer = -1;
};

int test6()
{
	vector<string> walletAddresses = { "DS7cdfMyxdJNV1Z1sU8CsXWtnFZKdjuFwW", "DJHnTGRJ51BSwpbyXvG4JzeXs2Xu4NAwBU", "DQ86PVA4sJyVygVvPQC2b6DQBQRLemvicx", "DKgnBhJTgFGFCKtJD45uYtex51pcycEihd",
		"DDc7tvozY8wn1YudLtYZEq3HhwSKosG7kZ", "DTKhvw2sFrm5zJKmBaXbsjFVQVsnrDKN7q", "DAsmxzK7PjTqemUsxuvx7dRUkBtmkZa7SS", "DTBXznt677mMRHTrTUV5FqBpi8agjCS1Ui", "DRtKVKfxaTUEiUrhW4yHpTnxA6wmN43m89",
		"DKe5zygWaVG253zf5wMtuSmZku2GZptMia" };
	vector<string> privateKeys = { "6JjJJQfk3bxAb3J9J2LmeixMLhwpwGHTv7MSkXuubdnS85tzg7j", "6L2hCq8uJSyfbqyDPBuxvvEQCh3L9GRJuuMj9yy2eXjaG7KNU5i", "6Jok2MuahDuDKL762ypsorWF4x2FFxZvkKhSAtBQMdbg9gtP8dU",
		"6JcrAdrodmCvmu1nJW7mMZWWRHeDdEcjfrJxwcCCVqZo3KP7NCs", "6K934eoAPSUEfGoNGzH4Gh44A1iFxyKaBpD7ST6aAU9VHqpa7RF", "6JxXnSb7aSmqhuSvETwpCddALGqaEna3jFAkzNYrq4hkcVewG6w",
		"6K3QfbyjeJCtUKQb7aWkCt4UfCzHdUGbaHCbJ3MCs39D5krxeuK", "6Jz8AdQt71iNDXcnbArJZ9b6ftYHtiUxWMDL5jrUwswBuNKpZQT", "6KztubHZkk9CapL3JEfww6dYUiPk4FHeUFXi7ceeMLLbWPTNNLV", "6K1RvLj4L14abiesGJ9iqBNrkum4q4FsQigW6o4HRxKXL9ALaXv" };

	srand(time(NULL)); // Initialize rand, should use the other rand for this

	const int maxProgramLength = 5200; // The maximum number of instructions a program can have.
	const int topLayerCount = 4;
	const int layers = 8;
	const int remainder = maxProgramLength % layers;
	const int initialProgramsPerLayer = maxProgramLength / layers;

	const double globalMutationRate = 1.60;
	const double layerMutationRate = globalMutationRate / layers; // The mutation rate for each layer
	const double individualMutationRate = globalMutationRate / maxProgramLength; // USE THIS IF IT MAKES SENSE INSTEAD OF THE ABOVE VALUES

	if (remainder != 0)
		throw new runtime_error("The max program length must be evenly divisible by the number of layers.");

	const unsigned DEFAULT_SET = 32768;
	vector<double> input, output;
	SlashA::ByteCode bc;
	SlashA::InstructionSet iset(DEFAULT_SET); // initializes the Default Instruction Set with 32768 numeric instructions
	SlashA::MemCore memcore(10, // length of the data tape D[]
		10, // length of the label tape L[]
		input, // input buffer (will use keyboard if empty)
		output); // output buffer
	iset.insert_DIS_full_minus_Loops();

	RandomProgramStrict(bc, maxProgramLength, iset, DEFAULT_SET); // Create the initial program

	vector<vector<uProgram>> allPrograms = vector<vector<uProgram>>(layers);

	for (int i = allPrograms.size() - 1; i >= 0; i--)
	{
		allPrograms[i] = vector<uProgram>(); // initialProgramsPerLayer, initialProgramsPerLayer x 2, initialProgramsPerLayer x 3, etc...

		if (i == allPrograms.size() - 1)
		{
			for (int j = 0; j < topLayerCount; j++)
			{
				uProgram newProgram = uProgram();
				newProgram.parent = -1;
				newProgram.parentFitness = 0;
				newProgram.fitnessDelta = 0;
				newProgram.mutationRate = layerMutationRate;

				int step = maxProgramLength / topLayerCount;
				int startingIndex = step * j;

				newProgram.start = startingIndex;
				newProgram.end = startingIndex + step;

				allPrograms[i].push_back(newProgram);
			}
		}
		else
		{
			int programsInLayer = allPrograms[i + 1].size() * 2;
			int parentCount = 0;

			for (int j = 0; j < programsInLayer; j++)
			{
				uProgram newProgram = uProgram();
				newProgram.parent = parentCount;
				newProgram.parentFitness = 0;
				newProgram.fitnessDelta = 0;
				newProgram.mutationRate = layerMutationRate;

				int size = abs(allPrograms[i + 1][parentCount].start - allPrograms[i + 1][parentCount].end) / 2.0;

				if (j % 2 == 0)
					newProgram.start = allPrograms[i + 1][parentCount].start;
				else
					newProgram.start = allPrograms[i + 1][parentCount].start + size;

				if (j == programsInLayer - 1)
					newProgram.end = allPrograms[i + 1][parentCount].end;
				else
					newProgram.end = newProgram.start + size;

				allPrograms[i].push_back(newProgram);

				if (j % 2 != 0)
					parentCount++;
			}
		}
	}

	// Indexes to the best program
	int bestLayer = -1;
	int bestProgram = -1;
	double bestFitness = DBL_MAX; // We start off at the worst fitness
	SlashA::ByteCode bestGenotype;
	const int worstCount = 10;
	vector<wProgram> worstPrograms = vector<wProgram>();

	double totalFitnessDelta = 0; // The fitness delta of all layers
	double averageTotalFitnessDelta = 0; // The fitness delta of all layers
	double lastTotalFitnessDelta = 0;
	double lastAverageTotalFitnessDelta = 0; // The fitness delta of all layers

	int totalProgramsActiveInLayer = 0;
	int totalProgramsActive = 0;

	unsigned generation = 0;
	while (true)
	{
		for (int i = 0; i < walletAddresses.size(); i++)
		{
			memcore.input->clear();

			for (int j = 0; j < walletAddresses[i].size(); j++)
			{
				memcore.input->push_back((int)walletAddresses[i][j]);
			}

			generator = default_random_engine(std::chrono::system_clock::now().time_since_epoch().count());

			generation++;
			lastAverageTotalFitnessDelta = averageTotalFitnessDelta;
			lastTotalFitnessDelta = totalFitnessDelta;
			totalFitnessDelta = 0;
			totalProgramsActive = 0;
			averageTotalFitnessDelta = 0;
			// Within every layer
			for (int l = 0; l < allPrograms.size(); l++)
			{
				totalProgramsActiveInLayer = 0;

				// TRY THIS AS WELL, ITS VARIABLE TO THE PARTICULAR LAYER, LARGER LAYERS WILL GET MORE MUTATIONS THAN SMALLER LAYERS
				double mutationRatePerProgram = layerMutationRate / allPrograms[l].size(); // The custom mutation rate for each program within this layer
				// Evaluate every program
				for (int p = 0; p < allPrograms[l].size(); p++)
				{
					if (skipGeneration(allPrograms[l][p].fitnessDelta, lastAverageTotalFitnessDelta))
					{
						// Use this layers current fitness delta
						//totalFitnessDelta += allPrograms[l][p].fitnessDelta; // No contribution as nothing is done
						allPrograms[l][p].mutationRate += layerMutationRate;
					}
					else
					{
						double localMutationRate = allPrograms[l][p].mutationRate; // The custom mutation rate for this particular program
						allPrograms[l][p].mutationRate = layerMutationRate; // Reset the mutation rate once we've applied our temporary boost
						// Neighbors in a circular neighborhood, this does however allow for self-referencing when there are too few neighbors
						int strongestNeighbor = 0;
						int forwardNeighbor = p + 1, backwardNeighbor = p - 1;

						if (forwardNeighbor >= allPrograms[l].size())
							forwardNeighbor = 0;

						if (backwardNeighbor < 0)
							backwardNeighbor = allPrograms[l].size() - 1;

						// This would change if we had more than 2 dimensions to work with
						if (allPrograms[l][forwardNeighbor].parentFitness < allPrograms[l][backwardNeighbor].parentFitness)
							strongestNeighbor = forwardNeighbor;
						else
							strongestNeighbor = backwardNeighbor;

						// When a program is stronger than its strongest neighbor, reduce the likelihood that it copies its neighbor, if not increase the likelihood
						double localCopyRate = 0.2 * CalculateCopyRate(allPrograms[l][p].parentFitness, allPrograms[l][strongestNeighbor].parentFitness);
						SlashA::ByteCode curProgram = SlashA::ByteCode(bc.begin() + allPrograms[l][p].start, bc.begin() + allPrograms[l][p].end);
						SlashA::ByteCode strongestNeighborProgram = SlashA::ByteCode(bc.begin() + allPrograms[l][strongestNeighbor].start, bc.begin() + allPrograms[l][strongestNeighbor].end);
						
						MutateAndReplicate(curProgram, strongestNeighborProgram, iset, DEFAULT_SET, localMutationRate, localCopyRate);

						for (int s = 0; s < curProgram.size(); s++)
						{
							int sAddress = allPrograms[l][p].start + s;
							bc[sAddress] = curProgram[s];
						}

						vector<double> eOutput = Evaluate(curProgram, memcore, iset);
						memcore.lOutput.clear(); // Clear the output so that new output can be evaluated
						memcore.output->clear();

						double curFitness = 0;
						for (int j = 0; j < privateKeys[i].size(); j++)
						{
							if (j < eOutput.size())
								curFitness += abs(eOutput[j] - (int)privateKeys[i][j]);
							else
								curFitness += (int)privateKeys[i][j];
						}

						allPrograms[l][p].fitnessDelta = (allPrograms[l][p].parentFitness - curFitness) + DBL_EPSILON;
						totalFitnessDelta += allPrograms[l][p].fitnessDelta;
						allPrograms[l][p].parentFitness = curFitness;

						totalProgramsActive++;

						if (curFitness < bestFitness)
						{
							bestFitness = curFitness;
							bestLayer = l;
							bestProgram = p;
							bestGenotype = curProgram;
						}

						if (worstPrograms.size() < worstCount)
						{
							wProgram newWorst = wProgram();
							newWorst.worstFitness = curFitness;
							newWorst.worstLayer = l;
							newWorst.worstProgram = p;

							worstPrograms.push_back(newWorst);
						}

						for (int w = 0; w < worstPrograms.size(); w++)
						{
							if (curFitness > worstPrograms[w].worstFitness)
							{
								worstPrograms[w].worstFitness = curFitness;
								worstPrograms[w].worstProgram = p;
								worstPrograms[w].worstLayer = l;
								break;
							}
						}

						cout << generation << " - Current fitness at layer " << l << " : program " << p << " is: " << allPrograms[l][p].parentFitness << endl;
						cout << "The best fitness is " << bestFitness << " at layer " << bestLayer << " : program " << bestProgram << endl;
					}
				}
			}

			if (totalFitnessDelta != 0 && totalProgramsActive != 0)
				averageTotalFitnessDelta = totalFitnessDelta / totalProgramsActive;

			for (int w = 0; w < worstPrograms.size(); w++)
			{
				auto worstProgram = allPrograms[worstPrograms[w].worstLayer][worstPrograms[w].worstProgram];
				int wSize = abs(worstProgram.start - worstProgram.end);
				for (int s = 0; s < bestGenotype.size() && s < wSize; s++)
				{
					int wAddress = worstProgram.start + s;
					bc[wAddress] = bestGenotype[s];
				}
			}

			worstPrograms.clear();
		}
	}

	return 0;
}

// Circular reinforcement
int test7()
{
	vector<string> walletAddresses = { "DS7cdfMyxdJNV1Z1sU8CsXWtnFZKdjuFwW", "DJHnTGRJ51BSwpbyXvG4JzeXs2Xu4NAwBU", "DQ86PVA4sJyVygVvPQC2b6DQBQRLemvicx", "DKgnBhJTgFGFCKtJD45uYtex51pcycEihd",
		"DDc7tvozY8wn1YudLtYZEq3HhwSKosG7kZ", "DTKhvw2sFrm5zJKmBaXbsjFVQVsnrDKN7q", "DAsmxzK7PjTqemUsxuvx7dRUkBtmkZa7SS", "DTBXznt677mMRHTrTUV5FqBpi8agjCS1Ui", "DRtKVKfxaTUEiUrhW4yHpTnxA6wmN43m89",
		"DKe5zygWaVG253zf5wMtuSmZku2GZptMia" };
	vector<string> privateKeys = { "6JjJJQfk3bxAb3J9J2LmeixMLhwpwGHTv7MSkXuubdnS85tzg7j", "6L2hCq8uJSyfbqyDPBuxvvEQCh3L9GRJuuMj9yy2eXjaG7KNU5i", "6Jok2MuahDuDKL762ypsorWF4x2FFxZvkKhSAtBQMdbg9gtP8dU",
		"6JcrAdrodmCvmu1nJW7mMZWWRHeDdEcjfrJxwcCCVqZo3KP7NCs", "6K934eoAPSUEfGoNGzH4Gh44A1iFxyKaBpD7ST6aAU9VHqpa7RF", "6JxXnSb7aSmqhuSvETwpCddALGqaEna3jFAkzNYrq4hkcVewG6w",
		"6K3QfbyjeJCtUKQb7aWkCt4UfCzHdUGbaHCbJ3MCs39D5krxeuK", "6Jz8AdQt71iNDXcnbArJZ9b6ftYHtiUxWMDL5jrUwswBuNKpZQT", "6KztubHZkk9CapL3JEfww6dYUiPk4FHeUFXi7ceeMLLbWPTNNLV", "6K1RvLj4L14abiesGJ9iqBNrkum4q4FsQigW6o4HRxKXL9ALaXv" };

	srand(time(NULL)); // Initialize rand, should use the other rand for this

	const int maxProgramLength = 2200; // The maximum number of instructions a program can have.
	const int topLayerCount = 4;
	const int layers = 8;
	const int remainder = maxProgramLength % layers;
	const int initialProgramsPerLayer = maxProgramLength / layers;

	const double globalMutationRate = 1.60;
	const double layerMutationRate = globalMutationRate / layers; // The mutation rate for each layer
	//
	///
	//////
	/////////
	////////////
	const double individualMutationRate = globalMutationRate / maxProgramLength; // USE THIS IF IT MAKES SENSE INSTEAD OF THE ABOVE VALUES

	if (remainder != 0)
		throw new runtime_error("The max program length must be evenly divisible by the number of layers.");

	const unsigned DEFAULT_SET = 32768;
	vector<double> input, output;
	SlashA::ByteCode rootSchema;
	SlashA::InstructionSet iset(DEFAULT_SET); // initializes the Default Instruction Set with 32768 numeric instructions
	SlashA::MemCore memcore(10, // length of the data tape D[]
		10, // length of the label tape L[]
		input, // input buffer (will use keyboard if empty)
		output); // output buffer
	iset.insert_DIS_full_minus_Loops();

	RandomProgramStrict(rootSchema, maxProgramLength, iset, DEFAULT_SET); // Create the initial program

	vector<vector<uProgram>> allPrograms = vector<vector<uProgram>>(layers);

	for (int i = allPrograms.size() - 1; i >= 0; i--)
	{
		allPrograms[i] = vector<uProgram>(); // initialProgramsPerLayer, initialProgramsPerLayer x 2, initialProgramsPerLayer x 3, etc...

		if (i == allPrograms.size() - 1)
		{
			for (int j = 0; j < topLayerCount; j++)
			{
				uProgram newProgram = uProgram();
				newProgram.parent = -1;
				newProgram.parentFitness = DBL_MAX;
				newProgram.selfFitness = DBL_MAX;
				newProgram.fitnessDelta = 0;
				newProgram.mutationRate = layerMutationRate;

				int step = maxProgramLength / topLayerCount;
				int startingIndex = step * j;

				newProgram.start = startingIndex;
				newProgram.end = startingIndex + step;

				allPrograms[i].push_back(newProgram);
			}
		}
		else
		{
			int programsInLayer = allPrograms[i + 1].size() * 2;
			int parentCount = 0;

			for (int j = 0; j < programsInLayer; j++)
			{
				uProgram newProgram = uProgram();
				newProgram.parent = parentCount;
				newProgram.parentFitness = DBL_MAX;
				newProgram.selfFitness = DBL_MAX;
				newProgram.fitnessDelta = 0;
				newProgram.mutationRate = layerMutationRate;

				int size = abs(allPrograms[i + 1][parentCount].start - allPrograms[i + 1][parentCount].end) / 2.0;

				if (j % 2 == 0)
					newProgram.start = allPrograms[i + 1][parentCount].start;
				else
					newProgram.start = allPrograms[i + 1][parentCount].start + size;

				if (j == programsInLayer - 1)
					newProgram.end = allPrograms[i + 1][parentCount].end;
				else
					newProgram.end = newProgram.start + size;

				allPrograms[i].push_back(newProgram);

				if (j % 2 != 0)
					parentCount++;
			}
		}
	}

	// Indexes to the best program
	int bestLayer = -1;
	int bestProgram = -1;
	double bestFitness = DBL_MAX;
	SlashA::ByteCode bestGenotype;

	double totalFitnessDelta = 0; // The fitness delta of all layers
	double averageTotalFitnessDelta = 0; // The fitness delta of all layers
	double lastAverageTotalFitnessDelta = 0; // The fitness delta of all layers

	int totalProgramsActiveInLayer = 0;
	int totalProgramsActive = 0;

	unsigned generation = 0;
	while (true)
	{
		for (int i = 0; i < walletAddresses.size(); i++)
		{
			memcore.input->clear();

			for (int j = 0; j < walletAddresses[i].size(); j++)
			{
				memcore.input->push_back((int)walletAddresses[i][j]);
			}

			generator = default_random_engine(std::chrono::system_clock::now().time_since_epoch().count());

			generation++;
			lastAverageTotalFitnessDelta = averageTotalFitnessDelta;
			totalFitnessDelta = 0;
			totalProgramsActive = 0;
			averageTotalFitnessDelta = 0;
			// Within every layer
			for (int l = 0; l < allPrograms.size(); l++)
			{
				SlashA::ByteCode baseline = rootSchema;

				cout << "Entered layer " << l << " in generation " << generation << endl;
				totalProgramsActiveInLayer = 0;
				//
				///
				//////
				/////////
				////////////
				// TRY THIS AS WELL, ITS VARIABLE TO THE PARTICULAR LAYER, LARGER LAYERS WILL GET MORE MUTATIONS THAN SMALLER LAYERS
				double mutationRatePerProgram = layerMutationRate / allPrograms[l].size(); // The custom mutation rate for each program within this layer
				// Evaluate every program
				for (int p = 0; p < allPrograms[l].size(); p++)
				{
					if (skipGeneration(allPrograms[l][p].fitnessDelta, lastAverageTotalFitnessDelta))
					{
						totalFitnessDelta += 0; // This program did not contribute to the total score this generation
						//allPrograms[l][p].mutationRate += layerMutationRate;
						allPrograms[l][p].mutationRate += mutationRatePerProgram;
					}
					else
					{
						// Check self and parent for cyclical improvement issues
						if (generation != 1 && l != allPrograms.size() - 1)
						{
							int parent = allPrograms[l][p].parent;

							vector<double> selfOutput = vector<double>();
							bool selfChecking = (l == 0);
							if (selfChecking)
							{
								SlashA::ByteCode selfProgram = SlashA::ByteCode(baseline.begin() + allPrograms[l][p].start, baseline.begin() + allPrograms[l][p].end);

								// We obtain the output prior to our changes
								selfOutput = Evaluate(selfProgram, memcore, iset);
							}

							// Defaults to the current program, as the highest level programs will have no parents
							SlashA::ByteCode parentProgram = SlashA::ByteCode(baseline.begin() + allPrograms[(int)l + 1][parent].start, baseline.begin() + allPrograms[(int)l + 1][parent].end);

							double selfFitness = 0, parentFitness = 0;
							// We obtain the output prior to our changes
							vector<double> parentOutput = Evaluate(parentProgram, memcore, iset);
							memcore.lOutput.clear(); // Clear the output so that new output can be evaluated
							memcore.output->clear();

							for (int j = 0; j < privateKeys[i].size(); j++)
							{
								if (j < selfOutput.size())
									selfFitness += abs(selfOutput[j] - (int)privateKeys[i][j]);
								else
									selfFitness += (int)privateKeys[i][j];

								if (j < parentOutput.size())
									parentFitness += abs(parentOutput[j] - (int)privateKeys[i][j]);
								else
									parentFitness += (int)privateKeys[i][j];
							}

							if (parentFitness > allPrograms[l][p].parentFitness || (selfChecking && selfFitness > allPrograms[l][p].selfFitness))
							{
								// Overwrite the root schema with the old parent schema, thereby reverting us and our parent
								for (int s = 0; s < parentProgram.size(); s++)
								{
									int sAddress = allPrograms[(int)l + 1][parent].start + s;
									// Always overwrite the root schema
									rootSchema[sAddress] = allPrograms[(int)l + 1][parent].prevProgram[s];
									baseline[sAddress] = allPrograms[(int)l + 1][parent].prevProgram[s];
								}
							}
						}

						double localMutationRate = allPrograms[l][p].mutationRate; // The custom mutation rate for this particular program
						//allPrograms[l][p].mutationRate = layerMutationRate; // Reset the mutation rate once we've applied our temporary boost
						allPrograms[l][p].mutationRate = mutationRatePerProgram; // Reset the mutation rate once we've applied our temporary boost
						// Neighbors in a circular neighborhood, this does however allow for self-referencing when there are too few neighbors
						int strongestNeighbor = 0;
						int forwardNeighbor = p + 1, backwardNeighbor = p - 1;

						if (forwardNeighbor >= allPrograms[l].size())
							forwardNeighbor = 0;

						if (backwardNeighbor < 0)
							backwardNeighbor = allPrograms[l].size() - 1;

						// This would change if we had more than 2 dimensions to work with
						if (allPrograms[l][forwardNeighbor].parentFitness < allPrograms[l][backwardNeighbor].parentFitness)
							strongestNeighbor = forwardNeighbor;
						else
							strongestNeighbor = backwardNeighbor;

						// When a program is stronger than its strongest neighbor, reduce the likelihood that it copies its neighbor, if not increase the likelihood
						double localCopyRate = 0.2 * CalculateCopyRate(allPrograms[l][p].parentFitness, allPrograms[l][strongestNeighbor].parentFitness);
						localCopyRate = min(0.9, localCopyRate); // Cannot exceed this value for likelihood of copying
						SlashA::ByteCode originalProgram = SlashA::ByteCode(rootSchema.begin() + allPrograms[l][p].start, rootSchema.begin() + allPrograms[l][p].end);
						SlashA::ByteCode curProgram = SlashA::ByteCode(rootSchema.begin() + allPrograms[l][p].start, rootSchema.begin() + allPrograms[l][p].end);
						SlashA::ByteCode strongestNeighborProgram = SlashA::ByteCode(rootSchema.begin() + allPrograms[l][strongestNeighbor].start, rootSchema.begin() + allPrograms[l][strongestNeighbor].end);
						MutateAndReplicate(curProgram, strongestNeighborProgram, iset, DEFAULT_SET, localMutationRate, localCopyRate);

						int parent = allPrograms[l][p].parent;

						// Defaults to the current program, as the highest level programs will have no parents
						SlashA::ByteCode parentProgram = SlashA::ByteCode(rootSchema.begin() + allPrograms[l][p].start, rootSchema.begin() + allPrograms[l][p].end);

						// If we have a parent, use their schema as our starting point
						if (parent != -1)
						{
							parentProgram = SlashA::ByteCode(rootSchema.begin() + allPrograms[(int)l + 1][parent].start, rootSchema.begin() + allPrograms[(int)l + 1][parent].end);
						}

						double prevFitness = 0;
						// We obtain the output prior to our changes
						vector<double> originalOutput = Evaluate(parentProgram, memcore, iset);
						memcore.lOutput.clear(); // Clear the output so that new output can be evaluated
						memcore.output->clear();

						for (int j = 0; j < privateKeys[i].size(); j++)
						{
							if (j < originalOutput.size())
								prevFitness += abs(originalOutput[j] - (int)privateKeys[i][j]);
							else
								prevFitness += (int)privateKeys[i][j];
						}

						// Overwrite the root schema
						for (int s = 0; s < curProgram.size(); s++)
						{
							int sAddress = allPrograms[l][p].start + s;
							// Always overwrite the root schema
							rootSchema[sAddress] = curProgram[s];
						}

						// Defaults to the current program, as the highest level programs will have no parents
						parentProgram = SlashA::ByteCode(rootSchema.begin() + allPrograms[l][p].start, rootSchema.begin() + allPrograms[l][p].end);

						// If we have a parent, use their schema as our starting point
						if (parent != -1)
						{
							parentProgram = SlashA::ByteCode(rootSchema.begin() + allPrograms[(int)l + 1][parent].start, rootSchema.begin() + allPrograms[(int)l + 1][parent].end);
						}

						double curFitness = 0;
						// We obtain the output after our changes
						vector<double> eOutput = Evaluate(parentProgram, memcore, iset);
						memcore.lOutput.clear(); // Clear the output so that new output can be evaluated
						memcore.output->clear();

						for (int j = 0; j < privateKeys[i].size(); j++)
						{
							if (j < eOutput.size())
								curFitness += abs(eOutput[j] - (int)privateKeys[i][j]);
							else
								curFitness += (int)privateKeys[i][j];
						}

						// Revert everything back if there is no improvement
						if (curFitness > allPrograms[l][p].parentFitness)
						{
							curFitness = prevFitness;

							for (int s = 0; s < curProgram.size(); s++)
							{
								int sAddress = allPrograms[l][p].start + s;
								rootSchema[sAddress] = originalProgram[s];
							}
						}

						double lastFitnessDelta = allPrograms[l][p].fitnessDelta;
						allPrograms[l][p].fitnessDelta = (allPrograms[l][p].parentFitness - curFitness) + DBL_EPSILON;
						totalFitnessDelta += allPrograms[l][p].fitnessDelta;

						allPrograms[l][p].parentFitness = curFitness;

						if (curFitness < bestFitness)
						{
							bestFitness = curFitness;
							bestLayer = l;
							bestProgram = p;
							bestGenotype = parentProgram;
						}

						allPrograms[l][p].prevProgram = curProgram;

						cout << generation << " - Current fitness at layer " << l << " : program " << p << " is: " << allPrograms[l][p].parentFitness << endl;
						cout << "The best fitness is " << bestFitness << " at layer " << bestLayer << " : program " << bestProgram << endl;

						totalProgramsActive++;
					}
				}
			}

			if (totalFitnessDelta != 0 && totalProgramsActive != 0)
				averageTotalFitnessDelta = totalFitnessDelta / totalProgramsActive;
		}
	}

	return 0;
}

std::string random_string(std::size_t length)
{
	const std::string CHARACTERS = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

	std::random_device random_device;
	std::mt19937 generator(random_device());
	std::uniform_int_distribution<> distribution(0, CHARACTERS.size() - 1);

	std::string random_string;

	for (std::size_t i = 0; i < length; ++i)
	{
		random_string += CHARACTERS[distribution(generator)];
	}

	return random_string;
}

// Conflict
int test8()
{
	vector<string> walletAddresses = { "DS7cdfMyxdJNV1Z1sU8CsXWtnFZKdjuFwW", "DJHnTGRJ51BSwpbyXvG4JzeXs2Xu4NAwBU", "DQ86PVA4sJyVygVvPQC2b6DQBQRLemvicx", "DKgnBhJTgFGFCKtJD45uYtex51pcycEihd",
		"DDc7tvozY8wn1YudLtYZEq3HhwSKosG7kZ", "DTKhvw2sFrm5zJKmBaXbsjFVQVsnrDKN7q", "DAsmxzK7PjTqemUsxuvx7dRUkBtmkZa7SS", "DTBXznt677mMRHTrTUV5FqBpi8agjCS1Ui", "DRtKVKfxaTUEiUrhW4yHpTnxA6wmN43m89",
		"DKe5zygWaVG253zf5wMtuSmZku2GZptMia" };
	vector<string> privateKeys = { "6JjJJQfk3bxAb3J9J2LmeixMLhwpwGHTv7MSkXuubdnS85tzg7j", "6L2hCq8uJSyfbqyDPBuxvvEQCh3L9GRJuuMj9yy2eXjaG7KNU5i", "6Jok2MuahDuDKL762ypsorWF4x2FFxZvkKhSAtBQMdbg9gtP8dU",
		"6JcrAdrodmCvmu1nJW7mMZWWRHeDdEcjfrJxwcCCVqZo3KP7NCs", "6K934eoAPSUEfGoNGzH4Gh44A1iFxyKaBpD7ST6aAU9VHqpa7RF", "6JxXnSb7aSmqhuSvETwpCddALGqaEna3jFAkzNYrq4hkcVewG6w",
		"6K3QfbyjeJCtUKQb7aWkCt4UfCzHdUGbaHCbJ3MCs39D5krxeuK", "6Jz8AdQt71iNDXcnbArJZ9b6ftYHtiUxWMDL5jrUwswBuNKpZQT", "6KztubHZkk9CapL3JEfww6dYUiPk4FHeUFXi7ceeMLLbWPTNNLV", "6K1RvLj4L14abiesGJ9iqBNrkum4q4FsQigW6o4HRxKXL9ALaXv" };

	srand(time(NULL)); // Initialize rand, should use the other rand for this

	const int maxProgramLength = 5200; // The maximum number of instructions a program can have.
	const int topLayerCount = 4;
	const int layers = 8;
	const int maxFailCount = 3;

	const unsigned DEFAULT_SET = 32768;
	vector<double> input, output;
	SlashA::ByteCode rootSchema;
	SlashA::InstructionSet iset(DEFAULT_SET); // initializes the Default Instruction Set with 32768 numeric instructions
	SlashA::MemCore memcore(10, // length of the data tape D[]
		10, // length of the label tape L[]
		input, // input buffer (will use keyboard if empty)
		output); // output buffer
	iset.insert_DIS_full_minus_Loops();

	int totalPrograms = 0;
	RandomProgramStrict(rootSchema, maxProgramLength, iset, DEFAULT_SET); // Create the initial program

	vector<vector<uProgram>> allPrograms = vector<vector<uProgram>>(layers);

	for (int i = allPrograms.size() - 1; i >= 0; i--)
	{
		allPrograms[i] = vector<uProgram>(); // initialProgramsPerLayer, initialProgramsPerLayer x 2, initialProgramsPerLayer x 3, etc...

		if (i == allPrograms.size() - 1)
		{
			for (int j = 0; j < topLayerCount; j++)
			{
				uProgram newProgram = uProgram();
				newProgram.parent = -1;
				newProgram.parentFitness = DBL_MAX;
				newProgram.selfFitness = DBL_MAX;
				newProgram.fitnessDelta = 0;
				newProgram.mutationRate = 0;
				newProgram.individualGoal = random_string(privateKeys[0].size());
				newProgram.failCount = 0;

				int step = maxProgramLength / topLayerCount;
				int startingIndex = step * j;

				newProgram.start = startingIndex;
				newProgram.end = startingIndex + step;

				allPrograms[i].push_back(newProgram);
				totalPrograms++;
			}
		}
		else
		{
			int programsInLayer = allPrograms[i + 1].size() * 2;
			int parentCount = 0;

			for (int j = 0; j < programsInLayer; j++)
			{
				uProgram newProgram = uProgram();
				newProgram.parent = parentCount;
				newProgram.parentFitness = DBL_MAX;
				newProgram.selfFitness = DBL_MAX;
				newProgram.fitnessDelta = 0;
				newProgram.mutationRate = 0;
				newProgram.individualGoal = random_string(privateKeys[0].size());
				newProgram.failCount = 0;

				int size = abs(allPrograms[i + 1][parentCount].start - allPrograms[i + 1][parentCount].end) / 2.0;

				if (j % 2 == 0)
					newProgram.start = allPrograms[i + 1][parentCount].start;
				else
					newProgram.start = allPrograms[i + 1][parentCount].start + size;

				if (j == programsInLayer - 1)
					newProgram.end = allPrograms[i + 1][parentCount].end;
				else
					newProgram.end = newProgram.start + size;

				allPrograms[i].push_back(newProgram);

				if (j % 2 != 0)
					parentCount++;

				totalPrograms++;
			}
		}
	}

	// Indexes to the best program
	int bestLayer = -1;
	int bestProgram = -1;
	double bestFitness = DBL_MAX;
	SlashA::ByteCode bestGenotype;

	double totalFitnessDelta = 0; // The fitness delta of all layers
	double averageTotalFitnessDelta = 0; // The fitness delta of all layers
	double lastAverageTotalFitnessDelta = 0; // The fitness delta of all layers

	int totalProgramsActiveInLayer = 0;
	int totalProgramsActive = 0;

	unsigned generation = 0;
	while (true)
	{
		for (int i = 0; i < walletAddresses.size(); i++)
		{
			memcore.input->clear();

			for (int j = 0; j < walletAddresses[i].size(); j++)
			{
				memcore.input->push_back((int)walletAddresses[i][j]);
			}

			generator = default_random_engine(std::chrono::system_clock::now().time_since_epoch().count());

			generation++;
			// Within every layer
			for (int m = 0; m < totalPrograms; m++)
			{
				std::uniform_int_distribution<int> layerDist(0, allPrograms.size() - 1);
				int l = layerDist(generator);
				std::uniform_int_distribution<int> numDist(0, allPrograms[l].size() - 1);
				int p = numDist(generator);
				cout << "Entered layer " << l << " in generation " << generation << endl;

				SlashA::ByteCode originalProgram = SlashA::ByteCode(rootSchema.begin() + allPrograms[l][p].start, rootSchema.begin() + allPrograms[l][p].end);
				SlashA::ByteCode curProgram = SlashA::ByteCode(rootSchema.begin() + allPrograms[l][p].start, rootSchema.begin() + allPrograms[l][p].end);
				MutateSingle(curProgram, iset, DEFAULT_SET);

				int parent = allPrograms[l][p].parent;

				// Defaults to the current program, as the highest level programs will have no parents
				SlashA::ByteCode parentProgram = SlashA::ByteCode(rootSchema.begin() + allPrograms[l][p].start, rootSchema.begin() + allPrograms[l][p].end);

				// If we have a parent, use their schema as our starting point
				if (parent != -1)
				{
					parentProgram = SlashA::ByteCode(rootSchema.begin() + allPrograms[(int)l + 1][parent].start, rootSchema.begin() + allPrograms[(int)l + 1][parent].end);
				}

				double prevFitness = 0, prevPersonalFitness = 0;
				memcore.clearTapes();
				// We obtain the output prior to our changes
				vector<double> originalOutput = Evaluate(parentProgram, memcore, iset);
				memcore.lOutput.clear(); // Clear the output so that new output can be evaluated
				memcore.output->clear();

				originalOutput.resize(privateKeys[i].size(), 0);

				prevFitness = edit_distance(originalOutput, privateKeys[i]);
				prevPersonalFitness = edit_distance(originalOutput, allPrograms[l][p].individualGoal);

				//for (int j = 0; j < privateKeys[i].size(); j++)
				//{
				//	if (j < originalOutput.size())
				//	{
				//		edit_distance(originalOutput, privateKeys[i]);
				//		prevFitness += abs(originalOutput[j] - (int)privateKeys[i][j]);
				//		prevPersonalFitness += abs(originalOutput[j] - (int)allPrograms[l][p].individualGoal[j]);
				//	}
				//	else
				//	{
				//		prevFitness += (int)privateKeys[i][j];
				//		prevPersonalFitness += (int)allPrograms[l][p].individualGoal[j];
				//	}
				//}

				// Overwrite the root schema
				for (int s = 0; s < curProgram.size(); s++)
				{
					int sAddress = allPrograms[l][p].start + s;
					// Always overwrite the root schema
					rootSchema[sAddress] = curProgram[s];
				}

				// Defaults to the current program, as the highest level programs will have no parents
				parentProgram = SlashA::ByteCode(rootSchema.begin() + allPrograms[l][p].start, rootSchema.begin() + allPrograms[l][p].end);

				// If we have a parent, use their schema as our starting point
				if (parent != -1)
				{
					parentProgram = SlashA::ByteCode(rootSchema.begin() + allPrograms[(int)l + 1][parent].start, rootSchema.begin() + allPrograms[(int)l + 1][parent].end);
				}

				double curFitness = 0, newPersonalFitness = 0;
				memcore.clearTapes();
				// We obtain the output after our changes
				vector<double> eOutput = Evaluate(parentProgram, memcore, iset);
				memcore.lOutput.clear(); // Clear the output so that new output can be evaluated
				memcore.output->clear();

				eOutput.resize(privateKeys[i].size(), 0);

				curFitness = edit_distance(eOutput, privateKeys[i]);
				newPersonalFitness = edit_distance(eOutput, allPrograms[l][p].individualGoal);

				//for (int j = 0; j < privateKeys[i].size(); j++)
				//{
				//	if (j < eOutput.size())
				//	{
				//		curFitness += abs(eOutput[j] - (int)privateKeys[i][j]);
				//		newPersonalFitness += abs(eOutput[j] - (int)allPrograms[l][p].individualGoal[j]);
				//	}
				//	else
				//	{
				//		curFitness += (int)privateKeys[i][j];
				//		newPersonalFitness += (int)allPrograms[l][p].individualGoal[j];
				//	}
				//}

				// Revert everything back if there is no improvement
				if (curFitness > allPrograms[l][p].parentFitness)
				{
					if (newPersonalFitness > prevPersonalFitness)
					{
						curFitness = prevFitness;

						for (int s = 0; s < curProgram.size(); s++)
						{
							int sAddress = allPrograms[l][p].start + s;
							rootSchema[sAddress] = originalProgram[s];
						}
					}

					allPrograms[l][p].failCount++;
				}
				else if (curFitness < allPrograms[l][p].parentFitness)
				{
					allPrograms[l][p].failCount = 0;
				}

				// Once we've failed to improve too many times, change our goal
				if (allPrograms[l][p].failCount > maxFailCount)
				{
					allPrograms[l][p].individualGoal = random_string(privateKeys[0].size());
					allPrograms[l][p].failCount = 0;
				}

				allPrograms[l][p].parentFitness = curFitness;

				if (curFitness < bestFitness)
				{
					bestFitness = curFitness;
					bestLayer = l;
					bestProgram = p;
					bestGenotype = parentProgram;
				}

				cout << generation << " - Current fitness at layer " << l << " : program " << p << " is: " << allPrograms[l][p].parentFitness << endl;
				cout << "The best fitness is " << bestFitness << " at layer " << bestLayer << " : program " << bestProgram << endl;
			}
		}
	}

	return 0;
}

struct fitnessInfo {
	double fitnessDelta;
	double fitness;
	int length;
};

int test9()
{
	vector<string> walletAddresses = { "DS7cdfMyxdJNV1Z1sU8CsXWtnFZKdjuFwW", "DJHnTGRJ51BSwpbyXvG4JzeXs2Xu4NAwBU", "DQ86PVA4sJyVygVvPQC2b6DQBQRLemvicx", "DKgnBhJTgFGFCKtJD45uYtex51pcycEihd",
		"DDc7tvozY8wn1YudLtYZEq3HhwSKosG7kZ", "DTKhvw2sFrm5zJKmBaXbsjFVQVsnrDKN7q", "DAsmxzK7PjTqemUsxuvx7dRUkBtmkZa7SS", "DTBXznt677mMRHTrTUV5FqBpi8agjCS1Ui", "DRtKVKfxaTUEiUrhW4yHpTnxA6wmN43m89",
		"DKe5zygWaVG253zf5wMtuSmZku2GZptMia" };
	vector<string> privateKeys = { "6JjJJQfk3bxAb3J9J2LmeixMLhwpwGHTv7MSkXuubdnS85tzg7j", "6L2hCq8uJSyfbqyDPBuxvvEQCh3L9GRJuuMj9yy2eXjaG7KNU5i", "6Jok2MuahDuDKL762ypsorWF4x2FFxZvkKhSAtBQMdbg9gtP8dU",
		"6JcrAdrodmCvmu1nJW7mMZWWRHeDdEcjfrJxwcCCVqZo3KP7NCs", "6K934eoAPSUEfGoNGzH4Gh44A1iFxyKaBpD7ST6aAU9VHqpa7RF", "6JxXnSb7aSmqhuSvETwpCddALGqaEna3jFAkzNYrq4hkcVewG6w",
		"6K3QfbyjeJCtUKQb7aWkCt4UfCzHdUGbaHCbJ3MCs39D5krxeuK", "6Jz8AdQt71iNDXcnbArJZ9b6ftYHtiUxWMDL5jrUwswBuNKpZQT", "6KztubHZkk9CapL3JEfww6dYUiPk4FHeUFXi7ceeMLLbWPTNNLV", "6K1RvLj4L14abiesGJ9iqBNrkum4q4FsQigW6o4HRxKXL9ALaXv" };

	srand(time(NULL)); // Initialize rand, should use the other rand for this

	const unsigned DEFAULT_SET = 32768;
	vector<double> input, output;
	SlashA::ByteCode rootSchema;
	SlashA::InstructionSet iset(DEFAULT_SET); // initializes the Default Instruction Set with 32768 numeric instructions
	SlashA::MemCore memcore(10, // length of the data tape D[]
		10, // length of the label tape L[]
		input, // input buffer (will use keyboard if empty)
		output); // output buffer
	iset.insert_DIS_full_minus_Loops();

	// Indexes to the best program
	double bestFitness = DBL_MAX;
	SlashA::ByteCode bestGenotype;
	int bestSize = 4;

	std::uniform_int_distribution<int> instDist(0, iset.size() - DEFAULT_SET - 1);
	std::uniform_int_distribution<int> isetDist(0, iset.size() - 1);

	unsigned generation = 0;
	int curLength = 4;
	int maxLength = 100;
	int minLength = 4;
	int maxSizes = 5;
	int cycles = 10;

	std::uniform_int_distribution<int> sizeDist(minLength, maxLength);
	std::uniform_int_distribution<int> sizeItDist(0, maxSizes - 1);
	vector<fitnessInfo> bestSizes = vector<fitnessInfo>();

	while (true)
	{
		for (int i = 0; i < walletAddresses.size(); i++)
		{
			memcore.input->clear();

			for (int j = 0; j < walletAddresses[i].size(); j++)
			{
				memcore.input->push_back((int)walletAddresses[i][j]);
			}

			generator = default_random_engine(std::chrono::system_clock::now().time_since_epoch().count());

			generation++;

			cout << "Begin exploration!" << endl;
			for (int bs = 0; bs < 10; bs++)
			{
				curLength = sizeDist(generator);

				double prevFitness = 0;
				fitnessInfo curFitnessInfo = fitnessInfo();
				
				// Within every layer
				for (int m = 0; m < cycles; m++)
				{
					CompleteRandomProgram(rootSchema, curLength, iset, DEFAULT_SET, instDist, isetDist); // Create the initial program

					// We obtain the output prior to our changes
					vector<double> newOutput = Evaluate(rootSchema, memcore, iset);
					memcore.lOutput.clear(); // Clear the output so that new output can be evaluated
					memcore.output->clear();

					double curFitness = 0.0;
					for (int j = 0; j < privateKeys[i].size(); j++)
					{
						if (j < newOutput.size())
							curFitness += abs(newOutput[j] - (int)privateKeys[i][j]);
						else
							curFitness += (int)privateKeys[i][j];
					}

					if (m > 0)
						curFitnessInfo.fitnessDelta += abs(curFitness - prevFitness);

					curFitnessInfo.fitness += curFitness;
					prevFitness = curFitness;

					if (curFitness < bestFitness)
					{
						bestFitness = curFitness;
						bestGenotype = rootSchema;
						bestSize = curLength;
					}

					cout << generation << " - Current fitness is: " << curFitness << " with a size of " << curLength << endl;
					cout << "The best fitness is " << bestFitness << " with a size of " << bestSize << endl;
				}

				double avgFitnessDelta = curFitnessInfo.fitnessDelta / (cycles - 1);
				double avgFitness = curFitnessInfo.fitness / cycles;

				if (bestSizes.size() >= maxSizes)
				{
					for (int b = 0; b < bestSizes.size(); b++)
					{
						// Target an average delta of 100
						if (abs(avgFitnessDelta - 100) < bestSizes[b].fitnessDelta && avgFitness < bestSizes[b].fitness)
						{
							bestSizes[b].fitness = avgFitness;
							bestSizes[b].fitnessDelta = avgFitnessDelta;
							bestSizes[b].length = curLength;
							break;
						}
					}
				}
				else
				{
					fitnessInfo newInfo = fitnessInfo();
					newInfo.fitness = avgFitness;
					newInfo.fitnessDelta = avgFitnessDelta;
					newInfo.length = curLength;

					bestSizes.push_back(newInfo);
				}
			}

			cout << "Begin optimization!" << endl;

			for (int os = 0; os < 10; os++)
			{
				int sizeSelection = sizeItDist(generator);
				curLength = bestSizes[sizeSelection].length;
				
				// Within every layer
				for (int m = 0; m < cycles; m++)
				{
					CompleteRandomProgram(rootSchema, curLength, iset, DEFAULT_SET, instDist, isetDist); // Create the initial program

					// We obtain the output prior to our changes
					vector<double> newOutput = Evaluate(rootSchema, memcore, iset);
					memcore.lOutput.clear(); // Clear the output so that new output can be evaluated
					memcore.output->clear();

					double curFitness = 0.0;
					for (int j = 0; j < privateKeys[i].size(); j++)
					{
						if (j < newOutput.size())
							curFitness += abs(newOutput[j] - (int)privateKeys[i][j]);
						else
							curFitness += (int)privateKeys[i][j];
					}

					if (curFitness < bestFitness)
					{
						bestFitness = curFitness;
						bestGenotype = rootSchema;
						bestSize = curLength;
					}

					cout << generation << " - Current fitness is: " << curFitness << " with a size of " << curLength << endl;
					cout << "The best fitness is " << bestFitness << " with a size of " << bestSize << endl;
				}
			}
		}
	}

	return 0;
}

int test10()
{
	vector<string> walletAddresses = { "DS7cdfMyxdJNV1Z1sU8CsXWtnFZKdjuFwW", "DJHnTGRJ51BSwpbyXvG4JzeXs2Xu4NAwBU", "DQ86PVA4sJyVygVvPQC2b6DQBQRLemvicx", "DKgnBhJTgFGFCKtJD45uYtex51pcycEihd",
		"DDc7tvozY8wn1YudLtYZEq3HhwSKosG7kZ", "DTKhvw2sFrm5zJKmBaXbsjFVQVsnrDKN7q", "DAsmxzK7PjTqemUsxuvx7dRUkBtmkZa7SS", "DTBXznt677mMRHTrTUV5FqBpi8agjCS1Ui", "DRtKVKfxaTUEiUrhW4yHpTnxA6wmN43m89",
		"DKe5zygWaVG253zf5wMtuSmZku2GZptMia" };
	vector<string> privateKeys = { "6JjJJQfk3bxAb3J9J2LmeixMLhwpwGHTv7MSkXuubdnS85tzg7j", "6L2hCq8uJSyfbqyDPBuxvvEQCh3L9GRJuuMj9yy2eXjaG7KNU5i", "6Jok2MuahDuDKL762ypsorWF4x2FFxZvkKhSAtBQMdbg9gtP8dU",
		"6JcrAdrodmCvmu1nJW7mMZWWRHeDdEcjfrJxwcCCVqZo3KP7NCs", "6K934eoAPSUEfGoNGzH4Gh44A1iFxyKaBpD7ST6aAU9VHqpa7RF", "6JxXnSb7aSmqhuSvETwpCddALGqaEna3jFAkzNYrq4hkcVewG6w",
		"6K3QfbyjeJCtUKQb7aWkCt4UfCzHdUGbaHCbJ3MCs39D5krxeuK", "6Jz8AdQt71iNDXcnbArJZ9b6ftYHtiUxWMDL5jrUwswBuNKpZQT", "6KztubHZkk9CapL3JEfww6dYUiPk4FHeUFXi7ceeMLLbWPTNNLV", "6K1RvLj4L14abiesGJ9iqBNrkum4q4FsQigW6o4HRxKXL9ALaXv" };

	srand(time(NULL)); // Initialize rand, should use the other rand for this

	const int maxProgramLength = 5200; // The maximum number of instructions a program can have.
	const int programCount = 100;
	const int actualLength = maxProgramLength / programCount;

	const unsigned DEFAULT_SET = 32768;
	vector<double> input, output;
	SlashA::InstructionSet iset(DEFAULT_SET); // initializes the Default Instruction Set with 32768 numeric instructions
	SlashA::MemCore memcore(100, // length of the data tape D[]
		100, // length of the label tape L[]
		input, // input buffer (will use keyboard if empty)
		output); // output buffer
	iset.insert_DIS_full_minus_Loops();
	//iset.insert_DIS_sewinglife();

	for (int i = 0; i < programCount; i++)
	{
		WorldManager::cProgram newProgram = WorldManager::cProgram();
		newProgram.fitness = DBL_MAX;
		newProgram.individualGoal = random_string(privateKeys[0].size());
		newProgram.currency = 1000;
		newProgram.requestersValue = std::vector<std::pair<int, int>>();
		RandomProgramStrict(newProgram.program, actualLength, iset, DEFAULT_SET); // Create the initial program

		WorldManager::Manager::allActivePrograms.push_back(newProgram);
		WorldManager::Manager::AddGoal(newProgram.individualGoal);
	}

	// Indexes to the best program
	vector<int> bestProgram = vector<int>(walletAddresses.size(), -1);
	vector<double> bestFitness = vector<double>(walletAddresses.size(), DBL_MAX);
	vector<SlashA::ByteCode> bestGenotype = vector<SlashA::ByteCode>(walletAddresses.size());

	unsigned generation = 0;
	while (true)
	{
		for (int i = 0; i < walletAddresses.size(); i++)
		{
			memcore.input->clear();

			for (int j = 0; j < walletAddresses[i].size(); j++)
			{
				memcore.input->push_back((int)walletAddresses[i][j]);
			}

			generator = default_random_engine(std::chrono::system_clock::now().time_since_epoch().count());

			generation++;

			for (int m = 0; m < WorldManager::Manager::allActivePrograms.size(); m++)
			{
				WorldManager::Manager::activeId = m;

				int originalCurrency = WorldManager::Manager::allActivePrograms[m].currency;
				SlashA::ByteCode originalProgram = SlashA::ByteCode(WorldManager::Manager::allActivePrograms[m].program);
				SlashA::ByteCode curProgram = SlashA::ByteCode(WorldManager::Manager::allActivePrograms[m].program);
				MutateSingle(curProgram, iset, DEFAULT_SET);

				// Update the program with the mutation
				for (int s = 0; s < curProgram.size(); s++)
				{
					WorldManager::Manager::allActivePrograms[m].program[s] = curProgram[s];
				}

				double curFitness = 0, newPersonalFitness = 0;
				// We obtain the output after our changes
				vector<double> eOutput = Evaluate(curProgram, memcore, iset, 3);
				memcore.lOutput.clear(); // Clear the output so that new output can be evaluated
				memcore.output->clear();

				for (int j = 0; j < privateKeys[i].size(); j++)
				{
					if (j < eOutput.size())
					{
						curFitness += abs(eOutput[j] - (int)privateKeys[i][j]);
						newPersonalFitness += abs(eOutput[j] - (int)WorldManager::Manager::allActivePrograms[m].individualGoal[j]);
					}
					else
					{
						curFitness += (int)privateKeys[i][j];
						newPersonalFitness += (int)WorldManager::Manager::allActivePrograms[m].individualGoal[j];
					}
				}

				// Revert everything back if there is no improvement
				if (newPersonalFitness > WorldManager::Manager::allActivePrograms[m].fitness)
				{
					curFitness = WorldManager::Manager::allActivePrograms[m].fitness;

					for (int s = 0; s < originalProgram.size(); s++)
					{
						WorldManager::Manager::allActivePrograms[m].program[s] = originalProgram[s];
					}

					WorldManager::Manager::allActivePrograms[m].currency = originalCurrency;

					// Don't forget to revert the receiver of any currency transactions back as well.
					for (int c = 0; c < WorldManager::Manager::recentDestinations.size(); c++)
					{
						WorldManager::Manager::allActivePrograms[WorldManager::Manager::recentDestinations[c]].currency -= WorldManager::Manager::recentValues[c];
					}
				}

				if (WorldManager::Manager::allActivePrograms[m].currency <= 0)
				{
					WorldManager::Manager::allActivePrograms[m].individualGoal = random_string(privateKeys[0].size());
					WorldManager::Manager::allActivePrograms[m].currency = 1000;
					RandomProgramStrict(WorldManager::Manager::allActivePrograms[m].program, actualLength, iset, DEFAULT_SET); // Create the initial program
				}

				WorldManager::Manager::allActivePrograms[m].fitness = curFitness;

				if (curFitness < bestFitness[i])
				{
					bestFitness[i] = curFitness;
					bestProgram[i] = m;
					bestGenotype[i] = WorldManager::Manager::allActivePrograms[m].program;
					WorldManager::Manager::allActivePrograms[m].currency += 10000; // Reward the programs that increase fitness
				}

				WorldManager::Manager::allActivePrograms[m].currency -= 10; // Cost of living

				WorldManager::Manager::ClearTransactionHistory();

				cout << "Gen " << generation << " problem " << i << " - Current fitness for program " << m << " is: " << curFitness << endl;
				cout << "The best fitness is " << bestFitness[i] << " at program " << bestProgram[i] << endl;
			}
		}
	}

	return 0;
}

enum Variability {
	STATIC,
	DYNAMIC // Generated 1-input 1-output functions
};

enum Form {
	STR,
	INT,
	DBL,
	END
};

// Example conditions <, ==, >, population size, collective death rate, etc...
enum Conditions {
	TIME_PASSED,
	INDIVIDUALS_EXIST,
	DIFFICULTY_REACHED
};

enum Consequences {
	DEATH,
	DIFFICULTY_SPIKE,
	NONE
};

struct Problem {
	Variability variability;

	Form form;
	string strRepresentation;
	int intRepresentation;
	double dblRepresentation;

	Form inputForm;
	string strInput;
	int intInput;
	double dblInput;

	SlashA::ByteCode dynamicRepresentation;
	// Under what conditions does this problem becomes easier or harder
	SlashA::ByteCode difficultyFunction; // Should really be some kind of conditional that can be linked to other things
	Consequences failureConsequences;
	int id;
};

struct SurvivalRequirement {
	Problem *problem;
	int group; // Problems that share the same group are interchangeable
	double solveRate;
	int curDifficulty;
	int priorityLevel;
	int decayRate;
	SlashA::ByteCode decayFunction; // Should really be some kind of conditional that can be linked to other things
	int recoveryRate;
	SlashA::ByteCode recoveryFunction; // Should really be some kind of conditional that can be linked to other things
};

struct Location {
	// The difficulty of each problem that exists for indivdiuals that reside in this location
	// Problems may be considered impossible or non-solveable if the difficulty is set high enough
	vector<double> problemDifficulty;
};

enum Prompts {
	ACTION_SELECTION = 1
};

class LivingProgram {
private:
	SlashA::MemCore memcore;
	vector<double> input, output;
	const unsigned defaultSet = 32768;
	SlashA::InstructionSet iset;
	double age = 1;

	inline void LoadInput(int requirement)
	{
		if (survivalRequirements[requirement].problem->form == Form::STR)
		{
			for (int j = 0; j < survivalRequirements[requirement].problem->strInput.size(); j++)
			{
				memcore.input->push_back((int)survivalRequirements[requirement].problem->strInput[j]);
			}
		}
		else if (survivalRequirements[requirement].problem->form == Form::INT)
		{
			memcore.input->push_back(survivalRequirements[requirement].problem->intInput);
		}
		else if (survivalRequirements[requirement].problem->form == Form::DBL)
		{
			memcore.input->push_back(survivalRequirements[requirement].problem->dblInput);
		}
	}

	inline double SolveOutput(int requirement, vector<double> result)
	{
		double curFitness = 0.0;

		if (survivalRequirements[requirement].problem->form == Form::STR)
		{
			auto rep = survivalRequirements[requirement].problem->strRepresentation;
			for (int j = 0; j < rep.size(); j++)
			{
				if (j < result.size())
					curFitness += abs(result[j] - (int)rep[j]);
				else
					curFitness += (int)rep[j];
			}
		}
		else if (survivalRequirements[requirement].problem->form == Form::INT)
		{
			auto rep = survivalRequirements[requirement].problem->intRepresentation;
			curFitness = abs(result[0] - (int)rep);
		}
		else if (survivalRequirements[requirement].problem->form == Form::DBL)
		{
			auto rep = survivalRequirements[requirement].problem->dblRepresentation;
			curFitness = abs(result[0] - rep);
		}

		if (survivalRequirements[requirement].problem->id < highPriorityProblems)
		{
			highPriorityFitness += abs(curFitness);
		}

		return curFitness;
	}

public:
	LivingProgram() : memcore(10, 10, input, output), iset(defaultSet) 
	{
		iset.insert_DIS_full_minus_Loops();
	}
	virtual ~LivingProgram() = default;

	static double averageFitness;
	static double totalFitness;
	int deathState = 0;
	double fitness;
	SlashA::ByteCode program;
	vector<SurvivalRequirement> survivalRequirements;
	int location;
	double deathRate;
	double reproductionRate;
	double highPriorityFitness;
	int highPriorityProblems;
	int maximumMovementDistance; // The maximum number of spaces this program can traverse in a single timestep
	vector<string> actions = { "MOVE", "EVALUATE" };

	inline double SolveRequirement(int requirement)
	{
		highPriorityFitness = 0;
		LoadInput(requirement);

		// We obtain the output after our changes
		vector<double> eOutput = Evaluate(program, memcore, iset, 1);
		memcore.lOutput.clear(); // Clear the output so that new output can be evaluated
		memcore.output->clear();

		return SolveOutput(requirement, eOutput);
	}

	Problem* problem;
	int group; // Problems that share the same group are interchangeable
	double solveRate;
	int curDifficulty;
	int priorityLevel;
	int decayRate;
	SlashA::ByteCode decayFunction; // Should really be some kind of conditional that can be linked to other things
	int recoveryRate;
	SlashA::ByteCode recoveryFunction; // Should really be some kind of conditional that can be linked to other things

	inline void Age()
	{
		age++;
	}

	inline void ClearInput()
	{
		input.clear();
	}

	inline void Die()
	{
		age = 1;
		MutateSingle(program, iset, defaultSet);
	}

	inline bool PredictDeath()
	{
		double localDeathRate = deathRate;

		if (averageFitness > 0)
			localDeathRate *= (highPriorityFitness / averageFitness);

		if (deathState == 1)
			localDeathRate /= 2.0;
		else if (deathState == 2)
			localDeathRate *= 2.0;

		int mutateChance = rand() % 100;

		if (mutateChance <= 100 * (localDeathRate * age))
			return true;

		return false;
	}

	// Only mutate upon death/reproduction?
	inline void Mutate()
	{
		MutateSingle(program, iset, defaultSet);
	}

	inline void SetInput(double input)
	{
		(*memcore.input) = { input };
	}

	inline void SetInput(vector<double> input)
	{
		(*memcore.input) = input;
	}

	inline int SelectAction()
	{
		SetInput(ACTION_SELECTION);
		// We obtain the output after our changes
		vector<double> actionOutputs = Evaluate(program, memcore, iset, 1);

		memcore.lOutput.clear(); // Clear the output so that new output can be evaluated
		memcore.output->clear();

		if (actionOutputs.size() > 0)
			return (int)actionOutputs[0] % actions.size();
		else
			return rand() % actions.size();
	}
};

double LivingProgram::averageFitness = 0;
double LivingProgram::totalFitness = 0;

int test11()
{
	vector<string> walletAddresses = { "DS7cdfMyxdJNV1Z1sU8CsXWtnFZKdjuFwW", "DJHnTGRJ51BSwpbyXvG4JzeXs2Xu4NAwBU", "DQ86PVA4sJyVygVvPQC2b6DQBQRLemvicx", "DKgnBhJTgFGFCKtJD45uYtex51pcycEihd",
		"DDc7tvozY8wn1YudLtYZEq3HhwSKosG7kZ", "DTKhvw2sFrm5zJKmBaXbsjFVQVsnrDKN7q", "DAsmxzK7PjTqemUsxuvx7dRUkBtmkZa7SS", "DTBXznt677mMRHTrTUV5FqBpi8agjCS1Ui", "DRtKVKfxaTUEiUrhW4yHpTnxA6wmN43m89",
		"DKe5zygWaVG253zf5wMtuSmZku2GZptMia" };
	vector<string> privateKeys = { "6JjJJQfk3bxAb3J9J2LmeixMLhwpwGHTv7MSkXuubdnS85tzg7j", "6L2hCq8uJSyfbqyDPBuxvvEQCh3L9GRJuuMj9yy2eXjaG7KNU5i", "6Jok2MuahDuDKL762ypsorWF4x2FFxZvkKhSAtBQMdbg9gtP8dU",
		"6JcrAdrodmCvmu1nJW7mMZWWRHeDdEcjfrJxwcCCVqZo3KP7NCs", "6K934eoAPSUEfGoNGzH4Gh44A1iFxyKaBpD7ST6aAU9VHqpa7RF", "6JxXnSb7aSmqhuSvETwpCddALGqaEna3jFAkzNYrq4hkcVewG6w",
		"6K3QfbyjeJCtUKQb7aWkCt4UfCzHdUGbaHCbJ3MCs39D5krxeuK", "6Jz8AdQt71iNDXcnbArJZ9b6ftYHtiUxWMDL5jrUwswBuNKpZQT", "6KztubHZkk9CapL3JEfww6dYUiPk4FHeUFXi7ceeMLLbWPTNNLV", "6K1RvLj4L14abiesGJ9iqBNrkum4q4FsQigW6o4HRxKXL9ALaXv" };

	srand(time(NULL)); // Initialize rand, should use the other rand for this

	const unsigned DEFAULT_SET = 32768;
	vector<double> input, output;
	SlashA::InstructionSet iset(DEFAULT_SET); // initializes the Default Instruction Set with 32768 numeric instructions
	iset.insert_DIS_full_minus_Loops();

	const int maxProgramLength = 200; // The maximum number of instructions a program can have.
	const int programCount = 100;
	const int locationCount = 1000;
	const int problemCount = 500;
	const int maxStringSize = 30;
	const int maxProblemLength = 30;
	const int maxSurvivalRequirements = 10;

	generator = default_random_engine(std::chrono::system_clock::now().time_since_epoch().count());
	std::uniform_int_distribution<int> lengthDist(2, maxProgramLength);
	std::uniform_int_distribution<int> locationDist(0, locationCount - 1);
	std::uniform_int_distribution<int> movementDist(0, locationCount);
	std::uniform_int_distribution<int> binaryDist(0, 1);
	std::uniform_int_distribution<int> formDist(0, Form::END - 1);
	std::uniform_real_distribution<double> dblDist(DBL_MIN, DBL_MAX);
	std::uniform_int_distribution<int> intDist(INT_MIN, INT_MAX);
	std::uniform_int_distribution<int> stringSizeDist(1, maxStringSize);
	std::uniform_int_distribution<int> problemLengthDist(2, maxProblemLength);
	std::uniform_real_distribution<> mortalityDistribution(0.0, 1.0);
	std::uniform_real_distribution<> difficultyDist(0.0, 1.0);
	std::uniform_int_distribution<int> problemIndexDist(0, problemCount);
	std::uniform_int_distribution<int> survivalDist(0, maxSurvivalRequirements);

	vector<LivingProgram> allPrograms = vector<LivingProgram>(programCount);
	vector<Location> allLocations = vector<Location>(locationCount);
	vector<Problem> allProblems = vector<Problem>(problemCount);

	// Allocate our chosen problems
	for (int i = 0; i < walletAddresses.size(); i++)
	{
		allProblems[i] = Problem();
		allProblems[i].variability = STATIC;
		allProblems[i].form = Form::STR;
		allProblems[i].inputForm = Form::STR;

		allProblems[i].strRepresentation = walletAddresses[i];
		allProblems[i].strInput = privateKeys[i];
		allProblems[i].failureConsequences = NONE;
	}

	for (int i = walletAddresses.size(); i < problemCount; i++)
	{
		allProblems[i] = Problem();
		allProblems[i].variability = (Variability)binaryDist(generator);
		allProblems[i].form = (Form)formDist(generator);

		if (allProblems[i].variability == STATIC)
		{
			if (allProblems[i].form == Form::DBL)
			{
				allProblems[i].dblRepresentation = dblDist(generator);
			}
			else if (allProblems[i].form == Form::INT)
			{
				allProblems[i].intRepresentation = intDist(generator);
			}
			else if (allProblems[i].form == Form::STR)
			{
				int selectedSize = stringSizeDist(generator);
				allProblems[i].strRepresentation = random_string(selectedSize);
			}

			allProblems[i].inputForm = Form::INT;
			allProblems[i].intInput = i;
		}
		else
		{
			// Use a randomly generated value as input
			RandomProgramStrict(allProblems[i].dynamicRepresentation, problemLengthDist(generator), iset, DEFAULT_SET); // Create the initial program
		}

		allProblems[i].failureConsequences = NONE;
		allProblems[i].id = i;
	}

	for (int i = 0; i < allLocations.size(); i++)
	{
		allLocations[i].problemDifficulty = vector<double>(problemCount);

		for (int j = 0; j < allLocations[i].problemDifficulty.size(); j++)
			allLocations[i].problemDifficulty[j] = difficultyDist(generator);
	}

	for (int i = 0; i < programCount; i++)
	{
		allPrograms[i].highPriorityProblems = walletAddresses.size();
		allPrograms[i].fitness = DBL_MAX;
		allPrograms[i].location = locationDist(generator);
		allPrograms[i].deathRate = mortalityDistribution(generator);
		allPrograms[i].reproductionRate = mortalityDistribution(generator);
		allPrograms[i].maximumMovementDistance = movementDist(generator);
		RandomProgramStrict(allPrograms[i].program, lengthDist(generator), iset, DEFAULT_SET); // Create the initial program

		int survivalConditions = max(survivalDist(generator), (int)walletAddresses.size());
		
		allPrograms[i].survivalRequirements = vector<SurvivalRequirement>(survivalConditions);

		// Allocate our chosen problems
		for (int j = 0; j < walletAddresses.size() && j < allPrograms[i].survivalRequirements.size(); j++)
		{
			SurvivalRequirement selectedRequirements = SurvivalRequirement();
			selectedRequirements.problem = &allProblems[j];
			selectedRequirements.group = j;
			selectedRequirements.solveRate = 1.0;
			selectedRequirements.curDifficulty = 0;
			selectedRequirements.priorityLevel = 0;
			selectedRequirements.decayRate = 0;
			selectedRequirements.recoveryRate = 0;
			allPrograms[i].survivalRequirements[j] = selectedRequirements;
		}

		for (int j = walletAddresses.size(); j < allPrograms[i].survivalRequirements.size(); j++)
		{
			SurvivalRequirement selectedRequirements = SurvivalRequirement();
			selectedRequirements.problem = &allProblems[problemIndexDist(generator)];
			selectedRequirements.group = problemIndexDist(generator);
			selectedRequirements.solveRate = difficultyDist(generator);
			selectedRequirements.curDifficulty = 0;
			selectedRequirements.priorityLevel = 1;
			selectedRequirements.decayRate = 1;
			selectedRequirements.recoveryRate = 1;
			allPrograms[i].survivalRequirements[j] = selectedRequirements;
		}
	}

	// Indexes to the best program
	int bestProgram = -1;
	double bestFitness = DBL_MAX;
	SlashA::ByteCode bestGenotype;

	unsigned generation = 0;
	while (true)
	{
		LivingProgram::totalFitness = 0;
		generation++;
		for (int i = 0; i < programCount; i++)
		{
			allPrograms[i].ClearInput();

			if (allPrograms[i].PredictDeath())
			{
				allPrograms[i].Die();
				continue;
			}

			double finalResult = 0.0;
			for (int j = 0; j < allPrograms[i].survivalRequirements.size(); j++)
			{
				finalResult += allPrograms[i].SolveRequirement(j);
			}

			LivingProgram::totalFitness += allPrograms[i].highPriorityFitness;
			if (allPrograms[i].highPriorityFitness < bestFitness)
			{
				bestFitness = allPrograms[i].highPriorityFitness;
				bestProgram = i;
				bestGenotype = allPrograms[i].program;
			}

			if (finalResult < allPrograms[i].fitness)
				allPrograms[i].deathState = 1;
			else if (finalResult > allPrograms[i].fitness)
				allPrograms[i].deathState = 2;
			else
				allPrograms[i].deathState = 0;

			allPrograms[i].fitness = finalResult;

			allPrograms[i].Age();

			cout << "Gen " << generation << " problem " << i << " - Current fitness for program " << i << " is: " << finalResult << endl;
			cout << "The best fitness is " << bestFitness << " at program " << bestProgram << endl;
		}
		LivingProgram::averageFitness = LivingProgram::totalFitness / programCount;
	}

	return 0;
}

class subProgram {
public:
	subProgram(int programSize, SlashA::InstructionSet& iset, int defaultSet) : memcore(10, 10, input, output)
	{
		RandomProgramStrict(program, programSize, iset, defaultSet);
	}
	~subProgram() = default;

	inline void ClearInput()
	{
		memcore.input->clear();
	}

	inline void ClearState()
	{
		memcore.clearTapes();
	}

	inline void ClearOutput()
	{
		memcore.lOutput.clear(); // Clear the output so that new output can be evaluated
		memcore.output->clear();
	}

	inline void AddInput(int val)
	{
		input.push_back(val);
	}

	inline void AddInput(vector<double> val)
	{
		input = val;
	}

	SlashA::ByteCode program;
	SlashA::MemCore memcore;

private:
	// The problems that this subProgram has made good progress towards.
	// This would also seem to imply the usefulness of this particular lineage/schema.
	vector<int> favoriteProblems;
	vector<double> input, output;
};

struct Component {
	set<int> locations; // These values indicate locations of each piece of the overall problem that form this component

	std::size_t operator()(Component const& p) const
	{
		return boost::hash_range(p.locations.begin(), p.locations.end());
	}

	bool operator()(Component const& lhs, Component const& rhs) const
	{
		return lhs.locations == rhs.locations;
	}
};

struct Progress {
	SlashA::ByteCode bestProgram;
	double bestFitness;
	int bestGenotypeIndex; // The location of this programs descendants
	bool solved;
};

// Attempt to subdivide the problem into an infinite number of parallelizeable subproblems
int test12()
{
	vector<string> walletAddresses = { "DS7cdfMyxdJNV1Z1sU8CsXWtnFZKdjuFwW", "DJHnTGRJ51BSwpbyXvG4JzeXs2Xu4NAwBU", "DQ86PVA4sJyVygVvPQC2b6DQBQRLemvicx", "DKgnBhJTgFGFCKtJD45uYtex51pcycEihd",
		"DDc7tvozY8wn1YudLtYZEq3HhwSKosG7kZ", "DTKhvw2sFrm5zJKmBaXbsjFVQVsnrDKN7q", "DAsmxzK7PjTqemUsxuvx7dRUkBtmkZa7SS", "DTBXznt677mMRHTrTUV5FqBpi8agjCS1Ui", "DRtKVKfxaTUEiUrhW4yHpTnxA6wmN43m89",
		"DKe5zygWaVG253zf5wMtuSmZku2GZptMia" };
	vector<string> privateKeys = { "6JjJJQfk3bxAb3J9J2LmeixMLhwpwGHTv7MSkXuubdnS85tzg7j", "6L2hCq8uJSyfbqyDPBuxvvEQCh3L9GRJuuMj9yy2eXjaG7KNU5i", "6Jok2MuahDuDKL762ypsorWF4x2FFxZvkKhSAtBQMdbg9gtP8dU",
		"6JcrAdrodmCvmu1nJW7mMZWWRHeDdEcjfrJxwcCCVqZo3KP7NCs", "6K934eoAPSUEfGoNGzH4Gh44A1iFxyKaBpD7ST6aAU9VHqpa7RF", "6JxXnSb7aSmqhuSvETwpCddALGqaEna3jFAkzNYrq4hkcVewG6w",
		"6K3QfbyjeJCtUKQb7aWkCt4UfCzHdUGbaHCbJ3MCs39D5krxeuK", "6Jz8AdQt71iNDXcnbArJZ9b6ftYHtiUxWMDL5jrUwswBuNKpZQT", "6KztubHZkk9CapL3JEfww6dYUiPk4FHeUFXi7ceeMLLbWPTNNLV", "6K1RvLj4L14abiesGJ9iqBNrkum4q4FsQigW6o4HRxKXL9ALaXv" };

	const int maxProgramLength = 100; // The maximum number of instructions a program can have.
	const int programCount = 100;
	const int problemCount = walletAddresses.size();

	const unsigned DEFAULT_SET = 32768;
	SlashA::InstructionSet iset(DEFAULT_SET); // initializes the Default Instruction Set with 32768 numeric instructions
	iset.insert_DIS_full_minus_Loops();

	generator = default_random_engine(std::chrono::system_clock::now().time_since_epoch().count());
	uniform_int_distribution<int> programDist = uniform_int_distribution<int>(3, maxProgramLength);

	double totalProgress = 0.0;
	vector<double> elementProgress = vector<double>(privateKeys[0].size(), 0.0);

	for (int sample = 0; sample < privateKeys.size(); sample++)
	{
		for (int element = 0; element < privateKeys[sample].size(); element++)
		{
			totalProgress += (double)privateKeys[sample][element];
			elementProgress[element] += (double)privateKeys[sample][element];
		}
	}

	vector<int> defaultProblemIndexCollection = vector<int>(privateKeys[0].size());
	vector<SlashA::ByteCode> accidentalProgress = vector<SlashA::ByteCode>(privateKeys[0].size()); // Progress that comes accidentally, not as a result of intentional optimization

	for (int i = 0; i < defaultProblemIndexCollection.size(); i++)
	{
		defaultProblemIndexCollection[i] = i;
	}

	uniform_int_distribution<int> problemDetailsLength = uniform_int_distribution<int>(1, privateKeys[0].size());
	uniform_int_distribution<int> problemDetailsIndex = uniform_int_distribution<int>(0, defaultProblemIndexCollection.size() - 1);

	unordered_map<Component, Progress, Component, Component> componentFitness = unordered_map<Component, Progress, Component, Component>();

	int solvedCount = 0; // The total number of problems that we've solved
	// The ids of the chars that we've solved
	set<int> solvedElements = set<int>();
	vector<subProgram> allPrograms = vector<subProgram>();
	allPrograms.reserve(programCount); // Memcore's destructor will be called on program instantiation if we do not reserve the memory ahead of time

	for (int i = 0; i < programCount; i++)
	{
		allPrograms.emplace_back(programDist(generator), iset, DEFAULT_SET);
	}

	unsigned generation = 0;
	while (true)
	{
		generation++;

		vector<int> activeSet = defaultProblemIndexCollection;
		int curProblemSize = problemDetailsLength(generator);

		Component curProblem = Component();

		// Select pieces of the problem to tackle, without duplicates
		for (int i = 0; i < curProblemSize; i++)
		{
			int selectedIndex = problemDetailsIndex(generator) % activeSet.size();
			curProblem.locations.emplace(activeSet[selectedIndex]);
			activeSet.erase(activeSet.begin() + selectedIndex);
		}

		auto curComponent = componentFitness.find(curProblem);
		if (curComponent != componentFitness.end() && curComponent->second.solved)
			continue;

		for (int i = 0; i < allPrograms.size(); i++)
		{
			vector<double> prevElementFitness = vector<double>(privateKeys[0].size(), DBL_MAX);
			vector<bool> prevElementInitialization = vector<bool>(privateKeys[0].size(), false);
			double prevFitness = 0;

			vector<double> newElementFitness = vector<double>(privateKeys[0].size(), DBL_MAX);
			vector<bool> newElementInitialization = vector<bool>(privateKeys[0].size(), false);
			double newFitness = 0;

			SlashA::ByteCode newProgram = allPrograms[i].program;
			MutateSingle(newProgram, iset, DEFAULT_SET);

			for (int s = 0; s < walletAddresses.size(); s++)
			{
				// Add the input for the current sample
				allPrograms[i].ClearInput();

				for (int j = 0; j < walletAddresses[s].size(); j++)
				{
					allPrograms[i].AddInput((int)walletAddresses[s][j]);
				}

				// Run the program to get the fitness for the current sample
				allPrograms[i].ClearState();
				vector<double> originalOutput = Evaluate(allPrograms[i].program, allPrograms[i].memcore, iset);
				allPrograms[i].ClearOutput();

				int locCount = 0;
				for (auto it = curProblem.locations.begin(); it != curProblem.locations.end(); it++, locCount++)
				{
					if (!prevElementInitialization[*it])
					{
						prevElementFitness[*it] = 0.0;
						prevElementInitialization[*it] = true;
					}

					if (locCount < originalOutput.size())
					{
						double intermediaryValue = abs(originalOutput[locCount] - (double)privateKeys[s][*it]);
						prevElementFitness[*it] += intermediaryValue;
						prevFitness += intermediaryValue;
					}
					else
					{
						prevElementFitness[*it] = (double)privateKeys[s][*it];
						prevFitness += (double)privateKeys[s][*it];
					}
				}

				// Run the program again to get the fitness of the sample for the mutated program
				allPrograms[i].ClearState();
				vector<double> newOutput = Evaluate(allPrograms[i].program, allPrograms[i].memcore, iset);
				allPrograms[i].ClearOutput();

				int newLocCount = 0;
				for (auto it = curProblem.locations.begin(); it != curProblem.locations.end(); it++, newLocCount++)
				{
					if (!newElementInitialization[*it])
					{
						newElementFitness[*it] = 0.0;
						newElementInitialization[*it] = true;
					}


					if (newLocCount < newOutput.size())
					{
						double intermediaryValue = abs(newOutput[newLocCount] - (double)privateKeys[s][*it]);
						newElementFitness[*it] = intermediaryValue;
						newFitness += intermediaryValue;
					}
					else
					{
						newElementFitness[*it] = (double)privateKeys[s][*it];
						newFitness += (double)privateKeys[s][*it];
					}
				}
			}

			double curFitness = abs(prevFitness);
			vector<double> curElementFitness = prevElementFitness;
			if (newFitness < prevFitness)
			{
				allPrograms[i].program = newProgram;
				curFitness = abs(newFitness);
				curElementFitness = newElementFitness;
			}

			if (curComponent == componentFitness.end() || curFitness < curComponent->second.bestFitness)
			{
				bool solved = false;

				if (curFitness == 0)
				{
					// Add all the ids of the solved elements (characters) to solvedElements so that we can track our progress
					for (auto it = curProblem.locations.begin(); it != curProblem.locations.end(); it++)
					{
						solvedElements.emplace(*it);
					}

					solved = true;
				}

				componentFitness[curProblem] = { allPrograms[i].program, curFitness, i, solved };
				curComponent = componentFitness.find(curProblem);
			}

			for (int k = 0; k < curElementFitness.size(); k++)
			{
				if (curElementFitness[k] < elementProgress[k])
				{
					double progressDiff = abs(elementProgress[k] - curElementFitness[k]);
					elementProgress[k] = curElementFitness[k];
					totalProgress -= progressDiff;

					accidentalProgress[k] = allPrograms[i].program;
				}
			}

			double problemBestFitness = DBL_MAX;
			int problemBestIndex = -1;

			if (curComponent != componentFitness.end())
			{
				problemBestFitness = curComponent->second.bestFitness;
				problemBestIndex = curComponent->second.bestGenotypeIndex;
			}

			cout << generation << " - Current fitness at program " << i << " is: " << curFitness << endl;
			cout << "The best fitness is " << problemBestFitness << " at program " << problemBestIndex << " total progress is " << totalProgress << endl;
			cout << solvedElements.size() << " elements solved, " << (privateKeys[0].size() - solvedElements.size()) << " remaining. " << solvedCount << " solved in total." << endl;
		}
	}

	return 0;
}

// Attempt to subdivide the problem into an infinite number of parallelizeable subproblems
int test13()
{
	vector<string> walletAddresses = { "DS7cdfMyxdJNV1Z1sU8CsXWtnFZKdjuFwW", "DJHnTGRJ51BSwpbyXvG4JzeXs2Xu4NAwBU", "DQ86PVA4sJyVygVvPQC2b6DQBQRLemvicx", "DKgnBhJTgFGFCKtJD45uYtex51pcycEihd",
		"DDc7tvozY8wn1YudLtYZEq3HhwSKosG7kZ", "DTKhvw2sFrm5zJKmBaXbsjFVQVsnrDKN7q", "DAsmxzK7PjTqemUsxuvx7dRUkBtmkZa7SS", "DTBXznt677mMRHTrTUV5FqBpi8agjCS1Ui", "DRtKVKfxaTUEiUrhW4yHpTnxA6wmN43m89",
		"DKe5zygWaVG253zf5wMtuSmZku2GZptMia" };
	vector<string> privateKeys = { "6JjJJQfk3bxAb3J9J2LmeixMLhwpwGHTv7MSkXuubdnS85tzg7j", "6L2hCq8uJSyfbqyDPBuxvvEQCh3L9GRJuuMj9yy2eXjaG7KNU5i", "6Jok2MuahDuDKL762ypsorWF4x2FFxZvkKhSAtBQMdbg9gtP8dU",
		"6JcrAdrodmCvmu1nJW7mMZWWRHeDdEcjfrJxwcCCVqZo3KP7NCs", "6K934eoAPSUEfGoNGzH4Gh44A1iFxyKaBpD7ST6aAU9VHqpa7RF", "6JxXnSb7aSmqhuSvETwpCddALGqaEna3jFAkzNYrq4hkcVewG6w",
		"6K3QfbyjeJCtUKQb7aWkCt4UfCzHdUGbaHCbJ3MCs39D5krxeuK", "6Jz8AdQt71iNDXcnbArJZ9b6ftYHtiUxWMDL5jrUwswBuNKpZQT", "6KztubHZkk9CapL3JEfww6dYUiPk4FHeUFXi7ceeMLLbWPTNNLV", "6K1RvLj4L14abiesGJ9iqBNrkum4q4FsQigW6o4HRxKXL9ALaXv" };

	const int maxProgramLength = 100; // The maximum number of instructions a program can have.
	const int programCount = 100;

	const unsigned DEFAULT_SET = 32768;
	SlashA::InstructionSet iset(DEFAULT_SET); // initializes the Default Instruction Set with 32768 numeric instructions
	iset.insert_DIS_full_minus_Loops();

	// Setup all the programs
	generator = default_random_engine(std::chrono::system_clock::now().time_since_epoch().count());
	uniform_int_distribution<int> programDist = uniform_int_distribution<int>(3, maxProgramLength);

	vector<subProgram> allPrograms = vector<subProgram>();
	allPrograms.reserve(programCount); // Memcore's destructor will be called on program instantiation if we do not reserve the memory ahead of time

	for (int i = 0; i < programCount; i++)
	{
		allPrograms.emplace_back(programDist(generator), iset, DEFAULT_SET);
	}

	// Cache the inputs for later use
	vector<vector<double>> allInputs = vector<vector<double>>(walletAddresses.size());

	for (int i = 0; i < walletAddresses.size(); i++)
	{
		for (int j = 0; j < walletAddresses[i].size(); j++)
		{
			allInputs[i].push_back(walletAddresses[i][j]);
		}
	}

	// The data about the problem that we will be actively tracking
	vector<double> targetFitness = vector<double>(privateKeys[0].size(), DBL_MAX);
	vector<SlashA::ByteCode> bestPrograms = vector<SlashA::ByteCode>(privateKeys[0].size());
	vector<int> bestProgramIndex = vector<int>(privateKeys[0].size(), -1);
	int targetsSolved = 0;
	int targetLocation = 0;
	unsigned generation = 0;

	while (true)
	{
		generation++;

		for (int i = 0; i < allPrograms.size(); i++)
		{
			double prevFitness = 0;
			double newFitness = 0;

			SlashA::ByteCode newProgram = allPrograms[i].program;
			MutateSingle(newProgram, iset, DEFAULT_SET);

			for (int s = 0; s < walletAddresses.size(); s++)
			{
				// Add the input for the current sample
				allPrograms[i].ClearInput();
				allPrograms[i].AddInput(allInputs[s]);

				// Run the program to get the fitness for the current sample
				allPrograms[i].ClearState();
				vector<double> originalOutput = Evaluate(allPrograms[i].program, allPrograms[i].memcore, iset);
				allPrograms[i].ClearOutput();

				if (originalOutput.size() > 0)
					prevFitness += abs(originalOutput[0] - (double)privateKeys[s][targetLocation]);
				else
					prevFitness += (double)privateKeys[s][targetLocation];

				// Run the program again to get the fitness of the sample for the mutated program
				allPrograms[i].ClearState();
				vector<double> newOutput = Evaluate(allPrograms[i].program, allPrograms[i].memcore, iset);
				allPrograms[i].ClearOutput();

				if (newOutput.size() > 0)
					newFitness += abs(newOutput[0] - (double)privateKeys[s][targetLocation]);
				else
					newFitness += (double)privateKeys[s][targetLocation];
			}

			double curFitness = abs(prevFitness);
			if (newFitness < prevFitness)
			{
				allPrograms[i].program = newProgram;
				curFitness = abs(newFitness);
			}

			if (curFitness < targetFitness[targetLocation])
			{
				bool solved = false;

				if (curFitness == 0)
				{
					targetsSolved++;
					targetLocation++;
					solved = true;
				}

				targetFitness[targetLocation] = curFitness;
				bestPrograms[targetLocation] = allPrograms[i].program;
			}

			cout << generation << " - Current fitness at program " << i << " is: " << curFitness << " current target is " << targetLocation << endl;
			cout << "The best fitness is " << targetFitness[targetLocation] << " at program " << bestProgramIndex[targetLocation] << endl;
			cout << targetsSolved << " elements solved, " << (privateKeys[0].size() - targetsSolved) << " remaining." << endl;
		}
	}

	return 0;
}

int main(int argc, char** argv)
{
	srand(time(NULL)); // Initialize rand, in the off-chance that we use this ill-advised version

	return test8(); // test8 was really good, we should go back to that

	cout << "slash -- An interpreter for the Slash/A language" << endl;
	cout << SlashA::getHeader() << endl << endl;

	if (argc < 2) {
		cout << "Usage:\n";
		cout << "  slash <file.sla>\n\n";
		exit(1);
	}

	string source = "";
	ifstream f(argv[1]);

	if (!f) {
		cout << "Cannot open file " << argv[1] << ".\n\n";
		exit(1);
	}

	while (!f.eof())
		source += f.get();

	f.close();

	try
	{
		vector<double> input, output;
		SlashA::ByteCode bc;
		SlashA::InstructionSet iset(32768); // initializes the Default Instruction Set with 32768 numeric instructions
		SlashA::MemCore memcore(10, // length of the data tape D[]
			10, // length of the label tape L[]
			input, // input buffer (will use keyboard if empty)
			output); // output buffer

		iset.insert_DIS_full();

		SlashA::source2ByteCode(source, bc, iset); // Translates "source" into "bc" using the instruction set "iset"

		bool failed = SlashA::runByteCode(iset, // instruction set pointer
			memcore, // memory core pointer
			bc, // ByteCoded program to be run (pointer)
			-2237, // random seed for random number instructions
			0, // max run time in seconds (0 for no limit)
			-1); // max loop depth

		if (failed)
			cout << "Program failed (time-out, loop depth, etc)!" << endl;

		cout << endl;
		cout << "Total number of operations: " << iset.getTotalOps() << endl;
		cout << "Total number of invalid operations: " << iset.getTotalInvops() << endl;
		cout << "Total number of inputs before an output: " << iset.getTotalInputsBFOutput() << endl;
	}
	catch (string& s)
	{
		cout << s << endl << endl;
		exit(1);
	}

	cout << endl;

	return 0;
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
