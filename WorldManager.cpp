#include "WorldManager.h"

std::vector<WorldManager::cProgram> WorldManager::Manager::allActivePrograms = std::vector<WorldManager::cProgram>();
std::vector<std::string> WorldManager::Manager::allGoals = std::vector<std::string>();
std::vector<int> WorldManager::Manager::recentDestinations = std::vector<int>();
std::vector<int> WorldManager::Manager::recentValues = std::vector<int>();
int WorldManager::Manager::activeId = -1;