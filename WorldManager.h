#pragma once

#include <algorithm>
#include <iostream>
#include <fstream>
#include <string>
#include <time.h>
#include <set>
#include "lib/SlashA.hpp"
#include <random>
#include <chrono>
#include <map>

namespace WorldManager
{
	// Multiple goals, advanced goals, goal object
	struct cProgram {
		double fitness;
		SlashA::ByteCode program;
		std::string individualGoal;
		int currency;
		std::vector<std::pair<int, int>> requestersValue; // [index], [requester], [value]
	};

	class Manager
	{
	public:
		Manager() = default;
		~Manager() {};

		static std::vector<cProgram> allActivePrograms;
		static std::vector<std::string> allGoals;
		static std::vector<int> recentDestinations;
		static std::vector<int> recentValues;
		static int activeId;

		static bool Valid(int source, int target)
		{
			return target < allActivePrograms.size() && target > -1 && source < allActivePrograms.size() && source > -1;
		}

		static int CheckCurrencyRequestValue(int source, int index)
		{
			if (allActivePrograms[source].requestersValue.size() > index && index > -1)
			{
				return allActivePrograms[source].requestersValue[index].second;
			}

			return -1;
		}

		static int CheckCurrencyRequestId(int source, int index)
		{
			if (allActivePrograms[source].requestersValue.size() > index && index > -1)
			{
				return allActivePrograms[source].requestersValue[index].first;
			}

			return -1;
		}

		static void RemoveCurrencyRequest(int source, int index)
		{
			if (allActivePrograms[source].requestersValue.size() > index && index > -1)
			{
				if (allActivePrograms[source].requestersValue.size() >= 0)
					allActivePrograms[source].requestersValue = std::vector<std::pair<int, int>>();
				else
					allActivePrograms[source].requestersValue.erase(allActivePrograms[source].requestersValue.begin() + index);
			}
		}

		static void SendCurrencyRequest(int source, int target, int amount)
		{
			try
			{
				if (Valid(source, target))
				{
					if (!std::isfinite<double>(amount))
						return;

					allActivePrograms[target].requestersValue.push_back(std::pair<int, int>(source, amount));
				}
			}
			catch (std::exception e)
			{
				return; // Do nothing
			}
		}

		static void AddGoal(std::string goal)
		{
			if (count(allGoals.begin(), allGoals.end(), goal) == 0)
				allGoals.push_back(goal);
		}

		static int GetBalance(int source)
		{
			return allActivePrograms[source].currency;
		}

		static void ClearTransactionHistory()
		{
			recentDestinations.clear();
			recentValues.clear();
		}

		static void SendCurrency(int amount, int target, int source)
		{
			if (target < allActivePrograms.size() && target > -1 && source < allActivePrograms.size() && source > -1 && allActivePrograms[source].currency >= amount)
			{
				allActivePrograms[target].currency += amount;
				allActivePrograms[source].currency -= amount;

				recentDestinations.push_back(target);
				recentValues.push_back(amount);
			}
		}

		static int GetId(int source)
		{
			return source;
		}

		static int GetMyId()
		{
			return activeId;
		}

		static int GetFitness(int target)
		{
			if (target < allActivePrograms.size() && target > -1)
			{
				return allActivePrograms[target].fitness;
			}

			return -1;
		}

		static int GetGoal(int target)
		{
			if (target < allActivePrograms.size() && target > -1)
			{
				for (int i = 0; i < allGoals.size(); i++)
				{
					if (allGoals[i] == allActivePrograms[target].individualGoal)
						return i;
				}
			}

			return -1;
		}
	};
}